const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8889);

/* global describe beforeAll afterAll expect test */
beforeAll(async() => {
  await models.sequelize.sync({ force: true });
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

describe('signUp test', () => {
  test('signUp', async() => {
    const signUpResponse = await request(server)
      .post(`/api/v1/signUp`)
      .send({
        password: '1qaz!QAZ',
        name: 'User1',
        email: 'user1@gmail.com'
      });
    expect(signUpResponse.status).toEqual(200);
    const userDataResponse = await request(server)
      .get(`/api/v1/user`)
      .set('Authorization', `Bearer ${signUpResponse.body.token}`)
      .send();
    expect(userDataResponse.status).toEqual(200);
    expect(userDataResponse.text).toContain('user1@gmail.com');
  });
});

describe('get userData test', () => {
  test('after login', async() => {
    const loginResponse = await request(server)
      .post('/api/v1/login')
      .send({
        email: 'user1@gmail.com',
        password: '1qaz!QAZ'
      });
    const userDataResponse = await request(server)
      .get(`/api/v1/user`)
      .set('Authorization', `Bearer ${loginResponse.body.token}`)
      .send();

    expect(userDataResponse.status).toEqual(200);
    expect(userDataResponse.text).toContain('user1@gmail.com');
  });

  test('after logout', async() => {
    const loginResponse = await request(server)
      .post('/api/v1/login')
      .send({
        email: 'user1@gmail.com',
        password: '1qaz!QAZ'
      });
    const logoutResponse = await request(server)
      .get(`/api/v1/logout`)
      .set('Authorization', `Bearer ${loginResponse.body.token}`)
      .send();
    const userDataResponse = await request(server)
      .get(`/api/v1/user`)
      .set('Authorization', `Bearer ${logoutResponse.body.token}`)
      .send();
    expect(userDataResponse.status).toEqual(401);
    expect(userDataResponse.text).toContain('Unauthorized');
  });

  test('with wrong JWT', async() => {
    await request(server)
      .post('/api/v1/login')
      .send({
        email: 'user1@gmail.com',
        password: '1qaz!QAZ'
      });
    const userDataResponse = await request(server)
      .get(`/api/v1/user`)
      .set('Authorization', `Bearer wrongAccessToken`)
      .send();
    expect(userDataResponse.status).toEqual(401);
    expect(userDataResponse.text).toContain('Unauthorized');
  });

  test('after jwt time expires', async() => {
    await request(server)
      .post('/api/v1/login')
      .send({
        email: 'user1@gmail.com',
        password: '1qaz!QAZ'
      });
    const userDataResponse = await request(server)
      .get(`/api/v1/user`)
      .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTUwNjgxNTk4LCJleHAiOjE1NTA2ODE1OTh9.V5Trnc978KRklE0xazQOmuP7s0fuwzH6KF47mW3IWYI`)
      .send();
    expect(userDataResponse.status).toEqual(401);
    expect(userDataResponse.text).toContain('Unauthorized');
  });

  test('get new access token by refreshToken after jwt time expires', async() => {
    const loginResponse = await request(server)
      .post('/api/v1/login')
      .send({
        email: 'user1@gmail.com',
        password: '1qaz!QAZ'
      });
    const refreshTokenResponse = await request(server)
      .post('/api/v1/refreshToken')
      .send({
        refreshToken: loginResponse.body.refreshToken
      });
    const userDataResponse = await request(server)
      .get(`/api/v1/user`)
      .set('Authorization', `Bearer ${refreshTokenResponse.body.token}`)
      .send();
    expect(userDataResponse.status).toEqual(200);
    expect(userDataResponse.text).toContain('user1@gmail.com');
  });

  test('get new access token by wrong refreshToken', async() => {
    await request(server)
      .post('/api/v1/login')
      .send({
        email: 'user1@gmail.com',
        password: '1qaz!QAZ'
      });
    const refreshTokenResponse = await request(server)
      .post('/api/v1/refreshToken')
      .send({
        refreshToken: 'wrongRefreshToken'
      });
    expect(refreshTokenResponse.status).toEqual(403);
  });
});
