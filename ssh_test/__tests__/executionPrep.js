const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8889);

/* global beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  await models.sequelize.sync({ force: true });
  sandbox = await request(server)
    .post(`/api/v1/signUp`)
    .send({
      password: '1qaz!QAZ',
      name: 'User1',
      email: 'user1@gmail.com'
    });
  console.log('sandbox!' + sandbox.body.token);
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

test('Add two project', async() => {
  const userDataResponse = await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N1',
      description: 'Best project for fast exploration ...'
      // projectCases: JSON.stringify(['1']),
      // projectClients: JSON.stringify(['1', '2'])
    });
  expect(userDataResponse.status).toEqual(200);
  await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N2',
      description: 'Good project for thorough inspection ...'
      // projectClients: JSON.stringify(['1', '2'])
    });
  expect(userDataResponse.status).toEqual(200);
});

test('Add two Cases for project N2', async() => {
  // const firstResponse = await request(server)
  //   .post(`/api/v1/case`)
  //   .set('Authorization', `Bearer ${sandbox.body.token}`)
  //   .send({
  //     name: 'case N11',
  //     description: 'First case for ...',
  //     url: 'http://www.elamalta.com/',
  //     ProjectId: 2
  //     // url: 'mail.ru',
  //   });
  // expect(firstResponse.status).toEqual(200);
  const secondResponse = await request(server)
    .post(`/api/v1/case`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'case N22',
      description: 'Second case for ...',
      url: 'dailyinfo.co.uk/english-language-schools',
      ProjectId: 2
      // nba.com/teams
    });
  expect(secondResponse.status).toEqual(200);
  // const thirdResponse = await request(server)
  //   .post(`/api/v1/case`)
  //   .set('Authorization', `Bearer ${sandbox.body.token}`)
  //   .send({
  //     name: 'case N33',
  //     description: 'Second case for ...',
  //     url: 'vocabulary.com/dictionary/study',
  //     ProjectId: 2
  //   });
  // expect(thirdResponse.status).toEqual(200);
  // url: 'vocabulary.com/dictionary/study'
  // url: 'advancedleadership.harvard.edu',
});
test('Add two Clients for project N4', async() => {
  const firstResponse = await request(server)
    .post(`/api/v1/client`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'client N1',
      browser: 'chrome',
      type: 'viewport',
      width: 1600,
      height: 1024,
      ProjectId: 2
    });
  expect(firstResponse.status).toEqual(200);
  // const secondResponse = await request(server)
  //   .post(`/api/v1/client`)
  //   .set('Authorization', `Bearer ${sandbox.body.token}`)
  //   .send({
  //     name: 'client N2',
  //     browser: 'chrome',
  //     type: 'emulation',
  //     device: 'Galaxy S5',
  //     ProjectId: 2
  //   });
  // expect(secondResponse.status).toEqual(200);
});
