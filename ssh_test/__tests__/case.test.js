const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8889);

/* global beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  await models.sequelize.sync({ force: true });
  sandbox = await request(server)
    .post(`/api/v1/signUp`)
    .send({
      password: '1qaz!QAZ',
      name: 'User1',
      email: 'user1@gmail.com'
    });
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

test('Add two project', async() => {
  const userDataResponse = await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N1',
      description: 'Best project for fast exploration ...'
    });
  expect(userDataResponse.status).toEqual(200);
  await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N2',
      description: 'Good project for thorough inspection ...'
    });
  expect(userDataResponse.status).toEqual(200);
});

test('Add two Cases for project N2', async() => {
  // expect(signUpResponse.status).toEqual(200);
  const firstResponse = await request(server)
    .post(`/api/v1/case`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'case N1',
      description: 'First case for ...',
      url: 'google.com',
      ProjectId: 2
    });
  expect(firstResponse.status).toEqual(200);
  const secondResponse = await request(server)
    .post(`/api/v1/case`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'case N2',
      description: 'Second case for ...',
      url: 'yahoo.com',
      ProjectId: 2
    });
  expect(secondResponse.status).toEqual(200);
});
test('Get Case list for project N2', async() => {
  const Response = await request(server)
    .get(`/api/v1/cases/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 1,
      'name': 'case N1',
      'description': 'First case for ...',
      'url': 'google.com'
    }, {
      'id': 2,
      'name': 'case N2',
      'description': 'Second case for ...',
      'url': 'yahoo.com'
    }
  ]
  );
});
test('Update Case N2', async() => {
  const Response = await request(server)
    .put(`/api/v1/case`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      id: 2,
      name: 'Modified case N2',
      description: 'Modified Second case for ...',
      url: 'yahoo.com'
    });
  expect(Response.status).toEqual(200);
});
test('Get Case N2', async() => {
  const Response = await request(server)
    .get(`/api/v1/case/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject(
    {
      'id': 2,
      'name': 'Modified case N2',
      'description': 'Modified Second case for ...',
      'url': 'yahoo.com'
    }
  );
});
test('Delete Case N2', async() => {
  const Response = await request(server)
    .delete(`/api/v1/case/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
});
test('Get Case list for project N2', async() => {
  const Response = await request(server)
    .get(`/api/v1/cases/2`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 1,
      'name': 'case N1',
      'description': 'First case for ...',
      'url': 'google.com'
    }
  ]
  );
});
