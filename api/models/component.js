const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('Component', {
    component: {
      type: Sequelize.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'component',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
