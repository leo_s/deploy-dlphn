const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('ProjectExclusion', {
    exclusion: {
      type: Sequelize.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'projectExclusion',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
