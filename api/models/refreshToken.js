const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('RefreshToken', {
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    token: {
      type: Sequelize.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  }, {
    tableName: 'refreshToken',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
