const Sequelize = require('sequelize');

module.exports = sequelize => {
  const Client = sequelize.define('Client', {
    name: {
      type: Sequelize.STRING(50),
      allowNull: false
    },
    browser: {
      type: Sequelize.ENUM,
      values: ['chrome', 'firefox']
    },
    type: {
      type: Sequelize.ENUM,
      values: ['viewport', 'emulation']
    },
    width: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: { min: 200, max: 2000 }
    },
    height: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: { min: 200, max: 1500 }
    },
    device: {
      type: Sequelize.ENUM,
      allowNull: true,
      values: [
        'Galaxy S5',
        'Pixel 2',
        'Pixel 2 XL',
        'iPhone 5',
        'iPhone SE',
        'iPhone 6',
        'iPhone 7',
        'iPhone 6 Plus',
        'iPhone X',
        'iPad',
        'iPad Pro',
        'Nexus 10'
      ]
    },
    firstUse: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {
    tableName: 'client',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
  Client.associate = function(models) {
    Client.hasMany(models.RunResult, {
      as: 'runResultClient'
    });
  };
  return Client;
};
