const Sequelize = require('sequelize');

module.exports = sequelize => {
  return sequelize.define('Workflow', {
    status: {
      type: Sequelize.ENUM,
      values: [
        'In Process',
        'Need Validation',
        'Pass',
        'Fail'
      ]
    },
    startTime: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: null
    },
    duration: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null
    }
  }, {
    tableName: 'workflow',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
};
