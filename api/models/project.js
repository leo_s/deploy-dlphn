const Sequelize = require('sequelize');

module.exports = sequelize => {
  const Project = sequelize.define('Project', {
    name: {
      type: Sequelize.STRING(50),
      allowNull: false
    },
    description: {
      type: Sequelize.STRING(200),
      allowNull: true
    }
  }, {
    tableName: 'project',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  Project.associate = function(models) {
    Project.hasMany(models.Case, {
      as: 'case'
    });
    Project.hasMany(models.Client, {
      as: 'client'
    });
    Project.hasMany(models.ProjectToken, {
      as: 'projectToken'
    });
    Project.hasMany(models.ProjectExclusion, {
      as: 'projectExclusion'
    });
    Project.hasOne(models.Workflow, {
      as: 'project'
    });
    Project.hasMany(models.RunResult, {
      as: 'runResultProject'
    });
  };

  return Project;
};
