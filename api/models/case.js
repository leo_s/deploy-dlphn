const Sequelize = require('sequelize');

module.exports = sequelize => {
  const Case = sequelize.define('Case', {
    name: {
      type: Sequelize.STRING(50),
      allowNull: false
    },
    description: {
      type: Sequelize.STRING(200),
      allowNull: true
    },
    url: {
      type: Sequelize.STRING(50),
      allowNull: false
    },
    firstUse: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {
    tableName: 'case',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  Case.associate = function(models) {
    Case.hasMany(models.Component, {
      as: 'component'
    });
    Case.hasMany(models.CaseExclusion, {
      as: 'caseExclusion'
    });
    Case.hasMany(models.RunResult, {
      as: 'runResultCase'
    });
  };

  return Case;
};
