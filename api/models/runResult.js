const Sequelize = require('sequelize');

module.exports = sequelize => {
  const RunResult = sequelize.define('RunResult', {
    firstUse: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    screenshotError: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    lastErrorMessage: {
      type: Sequelize.STRING(200),
      allowNull: true
    },
    mismatch: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null
    }
  }, {
    tableName: 'runResult',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
  RunResult.associate = function(models) {
    RunResult.belongsTo(models.Case, {
      as: 'Case'
    });
    RunResult.belongsTo(models.Client, {
      as: 'Client'
    });
  };
  return RunResult;
};
