const Sequelize = require('sequelize');

module.exports = sequelize => {
  const User = sequelize.define('User', {
    name: {
      type: Sequelize.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: Sequelize.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        notEmpty: true
      }
    },
    password: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  }, {
    tableName: 'user',
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  User.associate = function(models) {
    User.hasMany(models.Project, {
      as: 'project'
    });
  };

  return User;
};
