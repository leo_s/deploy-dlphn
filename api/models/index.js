const Sequelize = require('sequelize');

const sequelize = new Sequelize(
  process.env.DB_NAME || 'snap_checker',
  process.env.DB_USER_NAME || 'root',
  process.env.DB_USER_PASSWORD || 'root', {
    host: process.env.DB_HOST || 'db',
    port: process.env.DB_PORT || 3306,
    dialect: process.env.DB_DIALECT || 'mysql',
    logging: true,
    operatorsAliases: false
  });

const Case = require('./case')(sequelize);
const CaseExclusion = require('./caseExclusion')(sequelize);
const Client = require('./client')(sequelize);
const Component = require('./component')(sequelize);
const Project = require('./project')(sequelize);
const ProjectExclusion = require('./projectExclusion')(sequelize);
const ProjectToken = require('./projectToken')(sequelize);
const RefreshToken = require('./refreshToken')(sequelize);
const User = require('./user')(sequelize);
const WorkFlow = require('./workflow')(sequelize);
const RunResult = require('./runResult')(sequelize);

const models = {

  [Case.name]: Case,
  [CaseExclusion.name]: CaseExclusion,
  [Client.name]: Client,
  [Component.name]: Component,
  [Project.name]: Project,
  [ProjectExclusion.name]: ProjectExclusion,
  [ProjectToken.name]: ProjectToken,
  [RefreshToken.name]: RefreshToken,
  [User.name]: User,
  [WorkFlow.name]: WorkFlow,
  [RunResult.name]: RunResult

};

Object.keys(models).forEach(modelName => {
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequelize;

module.exports = models;
