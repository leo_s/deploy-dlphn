const passport = require('../middlewares/passport');
const models = require('../models');

const dbAddAction = async ctx => {
  let { name, description, url, ProjectId } = ctx.request.body;
  return models.Case.create({
    name,
    description,
    url,
    ProjectId
  });
};

const dbGetAction = ctx => {
  const number = ctx.params.number;
  return models.Case.find({
    where: {
      id: number
    },
    attributes: ['id', 'name', 'description', 'url']
  });
};

const dbGetListAction = async ctx => {
  const number = ctx.params.number;
  return models.Case.findAll({
    where: {
      ProjectId: number
    },
    attributes: ['id', 'name', 'description', 'url']
  });
};

const dbUpdateAction = async ctx => {
  let { id, name, description, url } = ctx.request.body;
  return models.Case.update({
    name,
    description,
    url,
    firstUse: true
  }, {
    where: { id }
  });
};

const dbDeleteAction = async ctx => {
  const number = ctx.params.number;
  return models.Case.destroy({
    where: {
      id: number
    }
  });
};

const routerAction = async(err, user, ctx, action) => {
  if (err) {
    ctx.throw(500, err.message);
  }
  if (user) {
    try {
      let dbAction;
      switch (action) {
        case 'addCase':
          dbAction = dbAddAction(ctx);
          break;
        case 'getCase':
          dbAction = dbGetAction(ctx, user);
          break;
        case 'getCases':
          dbAction = dbGetListAction(ctx, user);
          break;
        case 'updateCase':
          dbAction = dbUpdateAction(ctx, user);
          break;
        case 'deleteCase':
          dbAction = dbDeleteAction(ctx, user);
          break;
      }
      return await dbAction.catch(
        error => {
          ctx.throw(500, error);
        }
      );
    } catch (err) {
      ctx.throw(500, err);
    }
  } else {
    ctx.throw(401);
  }
};

const addCase = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'addCase');
    ctx.status = 200;
  })(ctx);
};

const getCases = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getCases');
    ctx.status = 200;
  })(ctx);
};

const getCase = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getCase');
    ctx.status = 200;
  })(ctx);
};

const updateCase = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'updateCase');
    ctx.status = 200;
  })(ctx);
};
const deleteCase = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'deleteCase');
    ctx.status = 200;
  })(ctx);
};

module.exports = {
  addCase,
  getCase,
  getCases,
  updateCase,
  deleteCase
};
