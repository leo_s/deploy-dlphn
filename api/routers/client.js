const passport = require('../middlewares/passport');
const models = require('../models');

const dbAddAction = async ctx => {
  let { name, browser, type, width, height, device, ProjectId } = ctx.request.body;
  return models.Client.create({
    name,
    browser,
    type,
    width,
    height,
    device,
    ProjectId
  });
};
const dbGetAction = ctx => {
  const number = ctx.params.number;
  return models.Client.find({
    where: {
      id: number
    },
    attributes: ['id', 'name', 'browser', 'type', 'width', 'height', 'device']
  });
};

const dbGetListAction = async ctx => {
  const number = ctx.params.number;
  return models.Client.findAll({
    where: {
      projectId: number
    },
    attributes: ['id', 'name', 'browser', 'type', 'width', 'height', 'device']
  });
};
const dbUpdateAction = async ctx => {
  let { id, name, browser, type, width, height, device } = ctx.request.body;
  return models.Client.update({
    name,
    browser,
    type,
    width,
    height,
    device,
    firstUse: true
  }, {
    where: { id }
  });
};
const dbDeleteAction = async ctx => {
  const number = ctx.params.number;
  return models.Client.destroy({
    where: {
      id: number
    }
  });
};

const routerAction = async(err, user, ctx, action) => {
  if (err) {
    ctx.throw(500, err.message);
  }
  if (user) {
    try {
      let dbAction;
      switch (action) {
        case 'addClient':
          dbAction = dbAddAction(ctx, user);
          break;
        case 'getClient':
          dbAction = dbGetAction(ctx);
          break;
        case 'getClients':
          dbAction = dbGetListAction(ctx);
          break;
        case 'updateClient':
          dbAction = dbUpdateAction(ctx, user);
          break;
        case 'deleteClient':
          dbAction = dbDeleteAction(ctx, user);
          break;
      }
      return await dbAction.catch(
        error => {
          ctx.throw(500, error);
        }
      );
    } catch (err) {
      ctx.throw(500, err);
    }
  } else {
    ctx.throw(401);
  }
};

const addClient = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'addClient');
    ctx.status = 200;
  })(ctx);
};
const getClient = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getClient');
    ctx.status = 200;
  })(ctx);
};
const getClients = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getClients');
    ctx.status = 200;
  })(ctx);
};
const updateClient = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'updateClient');
    ctx.status = 200;
  })(ctx);
};

const deleteClient = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'deleteClient');
    ctx.status = 200;
  })(ctx);
};

module.exports = {
  addClient,
  getClient,
  getClients,
  updateClient,
  deleteClient
};
