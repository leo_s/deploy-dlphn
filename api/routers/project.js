const passport = require('../middlewares/passport');
const models = require('../models');
const Sequelize = require('sequelize');
const uuid = require('uuid-v4');

const dbAddAction = async(ctx, user) => {
  let { name, description } = ctx.request.body;
  const result = await models.Project.create({
    name,
    description,
    UserId: user.id
  });
  const projectToken = uuid();
  await models.ProjectToken.create({
    token: projectToken,
    ProjectId: result.dataValues.id
  });
  return result.dataValues.id;
};

const dbGetListAction = async(ctx, user) => {
  return models.Project.findAll({
    where: {
      UserId: user.id
    },
    attributes: [
      'id',
      'name',
      'description',
      [Sequelize.col('project.status'), 'status'],
      [Sequelize.col('project.duration'), 'duration']
    ],
    includeIgnoreAttributes: false,
    include: [{
      model: models.Workflow,
      as: 'project'
    }]

  });
};

const dbGetAction = async(ctx, user) => {
  const number = ctx.params.number;
  return models.Project.find({
    where: {
      id: number
    },
    attributes: [
      'id',
      'name',
      'description'
    ],
    includeIgnoreAttributes: false
  });
};

const dbUpdateAction = async(ctx, user) => {
  let { id, name, description } = ctx.request.body;

  return models.Project.update({
    name,
    description
  }, {
    where: { id }
  });
};

const dbDeleteAction = async(ctx, user) => {
  const number = ctx.params.number;
  return models.Project.destroy({
    where: {
      id: number
    }
  });
};

const routerAction = async(err, user, ctx, action) => {
  if (err) {
    ctx.throw(500, err.message);
  }
  if (user) {
    try {
      let dbAction;
      switch (action) {
        case 'addProject':
          dbAction = dbAddAction(ctx, user);
          break;
        case 'getProjects':
          dbAction = dbGetListAction(ctx, user);
          break;
        case 'getProject':
          dbAction = dbGetAction(ctx, user);
          break;
        case 'updateProject':
          dbAction = dbUpdateAction(ctx, user);
          break;
        case 'deleteProject':
          dbAction = dbDeleteAction(ctx, user);
          break;
      }
      return await dbAction.catch(
        error => {
          ctx.throw(500, error);
        }
      );
    } catch (err) {
      ctx.throw(500, err);
    }
  } else {
    ctx.throw(401);
  }
};

const addProject = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'addProject');
    ctx.status = 200;
  })(ctx);
};
const getProject = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getProject');
    ctx.status = 200;
  })(ctx);
};
const getProjects = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getProjects');
    ctx.status = 200;
  })(ctx);
};

const updateProject = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'updateProject');
    ctx.status = 200;
  })(ctx);
};

const deleteProject = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'deleteProject');
    ctx.status = 200;
  })(ctx);
};

module.exports = {
  addProject,
  getProject,
  getProjects,
  updateProject,
  deleteProject
};
