const passport = require('../middlewares/passport');
const models = require('../models');
const uuid = require('uuid-v4');

const getProjectTokens = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        const ProjectId = ctx.params.number;
        ctx.body = await models.ProjectToken.findAll({
          where: { ProjectId },
          attributes: ['id', 'token']
        });
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const addProjectToken = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        let { ProjectId } = ctx.request.body;
        const projectToken = uuid();
        await models.ProjectToken.create({
          token: projectToken,
          ProjectId
        });
        ctx.body = `Token is added`;
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const deleteProjectToken = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        const id = ctx.params.tokenId;
        ctx.body = await models.ProjectToken.destroy({
          where: { id }
        });
        ctx.body = `Token N ${id} is deleted`;
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

module.exports = {
  getProjectTokens,
  addProjectToken,
  deleteProjectToken
};
