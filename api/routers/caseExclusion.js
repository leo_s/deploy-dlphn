const passport = require('../middlewares/passport');
const models = require('../models');

const dbAddAction = async ctx => {
  let { exclusion, CaseId } = ctx.request.body;
  return models.CaseExclusion.create({
    exclusion,
    CaseId
  });
};

const dbGetAction = ctx => {
  const number = ctx.params.number;
  return models.CaseExclusion.find({
    where: {
      id: number
    },
    attributes: ['id', 'exclusion']
  });
};

const dbGetListAction = async ctx => {
  const number = ctx.params.number;
  return models.CaseExclusion.findAll({
    where: {
      CaseId: number
    },
    attributes: ['id', 'exclusion']
  });
};

const dbUpdateAction = async ctx => {
  let { id, exclusion } = ctx.request.body;
  return models.CaseExclusion.update({
    exclusion
  }, {
    where: { id }
  });
};

const dbDeleteAction = async ctx => {
  const number = ctx.params.number;
  return models.CaseExclusion.destroy({
    where: {
      id: number
    }
  });
};

const routerAction = async(err, user, ctx, action) => {
  if (err) {
    ctx.throw(500, err.message);
  }
  if (user) {
    try {
      let dbAction;
      switch (action) {
        case 'addCaseExclusion':
          dbAction = dbAddAction(ctx);
          break;
        case 'getCaseExclusion':
          dbAction = dbGetAction(ctx, user);
          break;
        case 'getCaseExclusions':
          dbAction = dbGetListAction(ctx, user);
          break;
        case 'updateCaseExclusion':
          dbAction = dbUpdateAction(ctx, user);
          break;
        case 'deleteCaseExclusion':
          dbAction = dbDeleteAction(ctx, user);
          break;
      }
      return await dbAction.catch(
        error => {
          ctx.throw(500, error);
        }
      );
    } catch (err) {
      ctx.throw(500, err);
    }
  } else {
    ctx.throw(401);
  }
};

const addCaseExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'addCaseExclusion');
    ctx.status = 200;
  })(ctx);
};

const getCaseExclusions = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getCaseExclusions');
    ctx.status = 200;
  })(ctx);
};

const getCaseExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getCaseExclusion');
    ctx.status = 200;
  })(ctx);
};

const updateCaseExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'updateCaseExclusion');
    ctx.status = 200;
  })(ctx);
};

const deleteCaseExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'deleteCaseExclusion');
    ctx.status = 200;
  })(ctx);
};

module.exports = {
  addCaseExclusion,
  getCaseExclusion,
  getCaseExclusions,
  updateCaseExclusion,
  deleteCaseExclusion
};
