const Sequelize = require('sequelize');
const passport = require('../middlewares/passport');
const models = require('../models');
const { goToProcess } = require('../service/process');
const send = require('koa-send');
const fs = require('fs');
const { setWorkflow, checkToken, getRunResult, setFail, getStatus } = require('../service/dbOperation');

const executeProject = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        const projectId = ctx.params.number;
        setWorkflow(projectId, ctx);
        goToProcess(parseInt(projectId));
        ctx.body = `Project N ${projectId} is launched`;
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const byTokenExecuteProject = async ctx => {
  try {
    const ProjectId = ctx.params.number;
    const token = ctx.params.token;
    const tokenProjectId = await checkToken(token);
    if (tokenProjectId && tokenProjectId.dataValues.ProjectId === parseInt(ProjectId)) {
      setWorkflow(ProjectId, ctx);
      goToProcess(parseInt(ProjectId));
      ctx.body = `Project N ${ProjectId} is launched`;
      ctx.status = 200;
    } else {
      ctx.body = `Wrong token`;
      ctx.status = 401;
    }
  } catch (err) {
    ctx.throw(500, err);
  }
};

const byTokenGetStatus = async ctx => {
  try {
    const ProjectId = ctx.params.number;
    const token = ctx.params.token;
    const tokenProjectId = await checkToken(token);
    if (tokenProjectId && tokenProjectId.dataValues.ProjectId === parseInt(ProjectId)) {
      ctx.body = await getStatus(ProjectId);
      ctx.status = 200;
    } else {
      ctx.body = `Wrong token`;
      ctx.status = 401;
    }
  } catch (err) {
    ctx.throw(500, err);
  }
};

const getProjectResult = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        const projectId = ctx.params.number;
        ctx.body = await getRunResult(projectId);
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const getItemSnapshots = async ctx => {
  try {
    const type = ctx.params.type;
    const projectId = ctx.params.projectId;
    const caseId = ctx.params.caseId;
    const clientId = ctx.params.clientId;
    ctx.type = 'png';
    ctx.body = 'png file';
    await send(ctx, `snapShots/${type}/${projectId}/${caseId}.${clientId}.png`);
    ctx.status = 200;
  } catch (err) {
    ctx.throw(500, err);
  }
};

const aproveSnapshot = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        const ProjectId = ctx.params.projectId;
        const CaseId = ctx.params.caseId;
        const ClientId = ctx.params.clientId;
        fs.unlinkSync(`snapShots/previous/${ProjectId}/${CaseId}.${ClientId}.png`);
        fs.copyFileSync(
          `snapShots/current/${ProjectId}/${CaseId}.${ClientId}.png`,
          `snapShots/previous/${ProjectId}/${CaseId}.${ClientId}.png`);
        await models.RunResult.update({
          mismatch: 0
        }, {
          where: { ProjectId, CaseId, ClientId }
        });
        let mismatch = await models.RunResult.findAll({
          where: {
            ProjectId,
            mismatch: { [Sequelize.Op.gt]: 0 }
          },
          attributes: ['mismatch']
        });
        let status = '';
        if (mismatch && mismatch.length > 0) {
          status = 'Need Validation';
        } else {
          status = 'Pass';
        }
        await models.Workflow.update({
          status
        }, {
          where: { ProjectId }
        });
        ctx.body = 'Snapshot is approved';
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const setProjectFail = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        let { projectId } = ctx.request.body;
        setFail(projectId);
        ctx.body = `Project ${projectId} was failed`;
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const byTokenSetProjectFail = async ctx => {
  try {
    let { projectId } = ctx.request.body;
    const token = ctx.params.token;
    const tokenProjectId = await checkToken(token);
    if (tokenProjectId && tokenProjectId.dataValues.ProjectId === parseInt(projectId)) {
      setFail(projectId);
      ctx.body = `Project ${projectId} was failed`;
      ctx.status = 200;
    } else {
      ctx.body = `Wrong token`;
      ctx.status = 401;
    }
  } catch (err) {
    ctx.throw(500, err);
  }
};

module.exports = {
  executeProject,
  byTokenExecuteProject,
  byTokenGetStatus,
  getProjectResult,
  getItemSnapshots,
  aproveSnapshot,
  setProjectFail,
  byTokenSetProjectFail
};
