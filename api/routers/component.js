const passport = require('../middlewares/passport');
const models = require('../models');

const dbAddAction = async ctx => {
  let { component, CaseId } = ctx.request.body;
  return models.Component.create({
    component,
    CaseId
  });
};

const dbGetAction = ctx => {
  const number = ctx.params.number;
  return models.Component.find({
    where: {
      id: number
    },
    attributes: ['id', 'component']
  });
};

const dbGetListAction = async ctx => {
  const number = ctx.params.number;
  return models.Component.findAll({
    where: {
      CaseId: number
    },
    attributes: ['id', 'component']
  });
};

const dbUpdateAction = async ctx => {
  let { id, component } = ctx.request.body;
  return models.Component.update({
    component
  }, {
    where: { id }
  });
};
const dbDeleteAction = async ctx => {
  const number = ctx.params.number;
  return models.Component.destroy({
    where: {
      id: number
    }
  });
};

const routerAction = async(err, user, ctx, action) => {
  if (err) {
    ctx.throw(500, err.message);
  }
  if (user) {
    try {
      let dbAction;
      switch (action) {
        case 'addComponent':
          dbAction = dbAddAction(ctx);
          break;
        case 'getComponent':
          dbAction = dbGetAction(ctx, user);
          break;
        case 'getComponents':
          dbAction = dbGetListAction(ctx, user);
          break;
        case 'updateComponent':
          dbAction = dbUpdateAction(ctx, user);
          break;
        case 'deleteComponent':
          dbAction = dbDeleteAction(ctx, user);
          break;
      }
      return await dbAction.catch(
        error => {
          ctx.throw(500, error);
        }
      );
    } catch (err) {
      ctx.throw(500, err);
    }
  } else {
    ctx.throw(401);
  }
};

const addComponent = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'addComponent');
    ctx.status = 200;
  })(ctx);
};

const getComponents = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getComponents');
    ctx.status = 200;
  })(ctx);
};

const getComponent = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getComponent');
    ctx.status = 200;
  })(ctx);
};

const updateComponent = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'updateComponent');
    ctx.status = 200;
  })(ctx);
};

const deleteComponent = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'deleteComponent');
    ctx.status = 200;
  })(ctx);
};

module.exports = {
  addComponent,
  getComponent,
  getComponents,
  updateComponent,
  deleteComponent
};
