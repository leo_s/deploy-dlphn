const Router = require('koa-router');
const {
  signUp,
  login,
  refreshToken,
  logout
} = require('./auth');

const {
  user
} = require('./users');

const {
  addCase,
  getCase,
  getCases,
  updateCase,
  deleteCase
} = require('./case');

const {
  addCaseExclusion,
  getCaseExclusion,
  getCaseExclusions,
  updateCaseExclusion,
  deleteCaseExclusion
} = require('./caseExclusion');

const {
  addClient,
  getClient,
  getClients,
  updateClient,
  deleteClient
} = require('./client');

const {
  addComponent,
  getComponent,
  getComponents,
  updateComponent,
  deleteComponent
} = require('./component');

const {
  addProject,
  getProject,
  getProjects,
  updateProject,
  deleteProject
} = require('./project');

const {
  addProjectExclusion,
  getProjectExclusion,
  getProjectExclusions,
  updateProjectExclusion,
  deleteProjectExclusion
} = require('./projectExclusion');

const {
  getProjectTokens,
  addProjectToken,
  deleteProjectToken
} = require('./token');

const {
  logWorkflow,
  udateWorkflow
} = require('./workflow');

const {
  executeProject,
  byTokenExecuteProject,
  byTokenGetStatus,
  getProjectResult,
  getItemSnapshots,
  aproveSnapshot,
  setProjectFail,
  byTokenSetProjectFail
} = require('./execution');

const router = new Router();

router.post('/api/v1/signUp', signUp);
router.get('/api/v1/user', user);
router.post('/api/v1/login', login);
router.post('/api/v1/refreshToken', refreshToken);
router.get('/api/v1/logout', logout);

router.post('/api/v1/case', addCase);
router.get('/api/v1/case/:number', getCase);
router.get('/api/v1/cases/:number', getCases);
router.put('/api/v1/case', updateCase);
router.delete('/api/v1/case/:number', deleteCase);

router.post('/api/v1/caseExclusion', addCaseExclusion);
router.get('/api/v1/caseExclusion/:number', getCaseExclusion);
router.get('/api/v1/caseExclusions/:number', getCaseExclusions);
router.put('/api/v1/caseExclusion', updateCaseExclusion);
router.delete('/api/v1/caseExclusion/:number', deleteCaseExclusion);
//
router.post('/api/v1/client', addClient);
router.get('/api/v1/client/:number', getClient);
router.get('/api/v1/clients/:number', getClients);
router.put('/api/v1/client', updateClient);
router.delete('/api/v1/client/:number', deleteClient);
//
router.post('/api/v1/component', addComponent);
router.get('/api/v1/component/:number', getComponent);
router.get('/api/v1/components/:number', getComponents);
router.put('/api/v1/component', updateComponent);
router.delete('/api/v1/component/:number', deleteComponent);

router.post('/api/v1/project', addProject);
router.get('/api/v1/project/:number', getProject);
router.get('/api/v1/projects', getProjects);
router.put('/api/v1/project', updateProject);
router.delete('/api/v1/project/:number', deleteProject);

router.post('/api/v1/projectExclusion', addProjectExclusion);
router.get('/api/v1/projectExclusion/:number', getProjectExclusion);
router.get('/api/v1/projectExclusions/:number', getProjectExclusions);
router.put('/api/v1/projectExclusion', updateProjectExclusion);
router.delete('/api/v1/projectExclusion/:number', deleteProjectExclusion);

router.post('/api/v1/workflow', logWorkflow);
router.put('/api/v1/workflow', udateWorkflow);

router.get('/api/v1/execute/:number', executeProject);
router.get('/api/v1/execute/:number/:token', byTokenExecuteProject);
router.get('/api/v1/status/:number/:token', byTokenGetStatus);
router.get('/api/v1/result/:number', getProjectResult);
router.get('/api/v1/snapshots/:type/:projectId/:caseId/:clientId', getItemSnapshots);
router.get('/api/v1/approve/:projectId/:caseId/:clientId', aproveSnapshot);
router.put('/api/v1/fail', setProjectFail);
router.put('/api/v1/failByToken/:token', byTokenSetProjectFail);

router.get('/api/v1/projectTokens/:number', getProjectTokens);
router.post('/api/v1/projectToken', addProjectToken);
router.delete('/api/v1/projectToken/:tokenId', deleteProjectToken);

module.exports = router;
