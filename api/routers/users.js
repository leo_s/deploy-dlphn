const passport = require('../middlewares/passport');

const user = async ctx => {
  await passport.authenticate('jwt', { session: false }, (err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      ctx.body = user.dataValues.email;
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

module.exports = {
  user
};
