const passport = require('../middlewares/passport');
const models = require('../models');

const dbAddAction = async ctx => {
  let { exclusion, ProjectId } = ctx.request.body;
  return models.ProjectExclusion.create({
    exclusion,
    ProjectId
  });
};

const dbGetAction = ctx => {
  const number = ctx.params.number;
  return models.ProjectExclusion.find({
    where: {
      id: number
    },
    attributes: ['id', 'exclusion']
  });
};

const dbGetListAction = async ctx => {
  const number = ctx.params.number;
  return models.ProjectExclusion.findAll({
    where: {
      ProjectId: number
    },
    attributes: ['id', 'exclusion']
  });
};

const dbUpdateAction = async ctx => {
  let { id, exclusion } = ctx.request.body;
  return models.ProjectExclusion.update({
    exclusion
  }, {
    where: { id }
  });
};

const dbDeleteAction = async ctx => {
  const number = ctx.params.number;
  return models.ProjectExclusion.destroy({
    where: {
      id: number
    }
  });
};

const routerAction = async(err, user, ctx, action) => {
  if (err) {
    ctx.throw(500, err.message);
  }
  if (user) {
    try {
      let dbAction;
      switch (action) {
        case 'addProjectExclusion':
          dbAction = dbAddAction(ctx);
          break;
        case 'getProjectExclusion':
          dbAction = dbGetAction(ctx, user);
          break;
        case 'getProjectExclusions':
          dbAction = dbGetListAction(ctx, user);
          break;
        case 'updateProjectExclusion':
          dbAction = dbUpdateAction(ctx, user);
          break;
        case 'deleteProjectExclusion':
          dbAction = dbDeleteAction(ctx, user);
          break;
      }
      return await dbAction.catch(
        error => {
          ctx.throw(500, error);
        }
      );
    } catch (err) {
      ctx.throw(500, err);
    }
  } else {
    ctx.throw(401);
  }
};

const addProjectExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'addProjectExclusion');
    ctx.status = 200;
  })(ctx);
};

const getProjectExclusions = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getProjectExclusions');
    ctx.status = 200;
  })(ctx);
};

const getProjectExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    ctx.body = await routerAction(err, user, ctx, 'getProjectExclusion');
    ctx.status = 200;
  })(ctx);
};

const updateProjectExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'updateProjectExclusion');
    ctx.status = 200;
  })(ctx);
};

const deleteProjectExclusion = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    await routerAction(err, user, ctx, 'deleteProjectExclusion');
    ctx.status = 200;
  })(ctx);
};

module.exports = {
  addProjectExclusion,
  getProjectExclusion,
  getProjectExclusions,
  updateProjectExclusion,
  deleteProjectExclusion
};
