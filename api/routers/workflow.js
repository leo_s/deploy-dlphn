const passport = require('../middlewares/passport');
const models = require('../models');

const logWorkflow = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        let { projectId } = ctx.request.body;
        await models.Workflow.create({
          projectId
        }).catch(
          error => {
            ctx.throw(500, error);
          }
        );
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

const udateWorkflow = async ctx => {
  await passport.authenticate('jwt', { session: false }, async(err, user) => {
    if (err) {
      ctx.throw(500, err.message);
    }
    if (user) {
      try {
        let { projectId, status } = ctx.request.body;
        await models.Workflow.update({
          status
        }, {
          where: { projectId }
        }
        ).catch(
          error => {
            ctx.throw(500, error);
          }
        );
        ctx.status = 200;
      } catch (err) {
        ctx.throw(500, err);
      }
    } else {
      ctx.throw(401);
    }
  })(ctx);
};

module.exports = {
  logWorkflow,
  udateWorkflow
};
