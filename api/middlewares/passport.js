const passport = require('koa-passport');
const LocalStrategy = require('passport-local');
const db = require('../models');
const bcrypt = require('bcrypt');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

const jwtSecret = {
  secret: 'jwt-secret'
};

passport.use(
  'login',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      session: false
    },
    (email, password, done) => {
      try {
        db.User.findOne({
          where: {
            email
          }
        }).then(user => {
          if (user === null) {
            return done(null, false);
          }
          bcrypt.compare(password, user.password).then(response => {
            if (response !== true) {
              return done(null, false);
            }
            return done(null, user);
          });
        });
      } catch (err) {
        done(err);
      }
    })
);

passport.use(
  'jwt',
  new JWTstrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: jwtSecret.secret
    },
    (jwtPayload, done) => {
      try {
        db.User.findOne({
          where: {
            id: jwtPayload.id
          }
        }).then(user => {
          if (user) {
            done(null, user);
          } else {
            done(null, false);
          }
        });
      } catch (err) {
        done(err);
      }
    })
);

module.exports = passport;
