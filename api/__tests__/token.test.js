const { fetchTokenPair } = require('../routers/auth');

/* global expect test */
test('fetchTokenPair returns token and refreshToken', async() => {
  const TokenPair = await fetchTokenPair(1);
  expect(TokenPair.token).toMatch(/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9/);
});
