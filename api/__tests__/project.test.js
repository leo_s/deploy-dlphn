const request = require('supertest');
const app = require('../index.js');
const models = require('../models');
const server = app.listen(8889);

/* global beforeAll afterAll expect test sandbox */
/* globals sandbox:true */
beforeAll(async() => {
  await models.sequelize.sync({ force: true });
  sandbox = await request(server)
    .post(`/api/v1/signUp`)
    .send({
      password: '1qaz!QAZ',
      name: 'User1',
      email: 'user1@gmail.com'
    });
});

afterAll(() => {
  server.close();
  console.log('server closed!');
});

test('Add two project', async() => {
  const userDataResponse = await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N1',
      description: 'Best project for fast exploration ...'
    });
  expect(userDataResponse.status).toEqual(200);
  await request(server)
    .post(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      name: 'project N2',
      description: 'Good project for thorough inspection ...'
    });
  expect(userDataResponse.status).toEqual(200);
});

test('Get Project list', async() => {
  const Response = await request(server)
    .get(`/api/v1/projects`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 1,
      'name': 'project N1',
      'description': 'Best project for fast exploration ...'
    }, {
      'id': 2,
      'name': 'project N2',
      'description': 'Good project for thorough inspection ...'
    }
  ]
  );
});
test('Update Project N1', async() => {
  const Response = await request(server)
    .put(`/api/v1/project`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send({
      id: 1,
      name: 'Modified project N1',
      description: 'Modified Best project for fast exploration ...'
    });
  expect(Response.status).toEqual(200);
});

test('Get Project N1', async() => {
  const Response = await request(server)
    .get(`/api/v1/project/1`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject(
    {
      'id': 1,
      'name': 'Modified project N1',
      'description': 'Modified Best project for fast exploration ...'
    }
  );
});

test('Delete Project N1', async() => {
  const Response = await request(server)
    .delete(`/api/v1/project/1`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
});
test('Get Project list after removal', async() => {
  const Response = await request(server)
    .get(`/api/v1/projects`)
    .set('Authorization', `Bearer ${sandbox.body.token}`)
    .send();
  expect(Response.status).toEqual(200);
  expect(Response.body).toMatchObject([
    {
      'id': 2,
      'name': 'project N2',
      'description': 'Good project for thorough inspection ...'
    }
  ]
  );
});
