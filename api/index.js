'use strict';

const PORT = process.env.PORT || 8888;

const Koa = require('koa');
const models = require('./models');
const koaBody = require('koa-bodyparser');
const passport = require('./middlewares/passport');
const router = require('./routers');

const port = process.env.PORT || PORT;
const app = new Koa();

app.use(koaBody());
app.keys = ['secret'];

app.use(passport.initialize());
app.use(router.routes());
app.use(router.allowedMethods());

if (!module.parent) {
  (async() => {
    await models.sequelize.sync({ force: true });
  })();

  app.listen(port, () => {
  });
}

module.exports = app;
