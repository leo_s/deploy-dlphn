const Sequelize = require('sequelize');
const models = require('../models');

const setWorkflow = async(projectId, ctx) => {
  let coolStart = await models.Workflow.count({
    where: { projectId }
  });
  let startTime = new Date();
  if (coolStart > 0) {
    await models.Workflow.update({
      startTime,
      status: 'In Process'
    }, {
      where: { projectId }
    }).catch(
      error => {
        ctx.throw(500, error);
      }
    );
  } else {
    await models.Workflow.create({
      startTime,
      projectId,
      status: 'In Process'
    }).catch(
      error => {
        ctx.throw(500, error);
      }
    );
  }
};

const setItemFirstUse = async(ProjectId, CaseId, ClientId, firstUse) => {
  const item = await models.RunResult.findOne({
    where: { ProjectId, CaseId, ClientId }
  });
  if (item) {
    await models.RunResult.update({
      firstUse
    }, {
      where: { ProjectId, CaseId, ClientId }
    });
  } else {
    await models.RunResult.create({
      ProjectId,
      CaseId,
      ClientId
    });
  }
};

const setScreenshotError = async({ projectId: ProjectId, caseId: CaseId, clientId: ClientId }, message) => {
  await models.RunResult.update({
    screenshotError: true,
    lastErrorMessage: message.message
  }, {
    where: { ProjectId, CaseId, ClientId }
  });
};

const clearScreenshotError = async({ projectId: ProjectId, caseId: CaseId, clientId: ClientId }) => {
  await models.RunResult.update({
    screenshotError: false
  }, {
    where: { ProjectId, CaseId, ClientId }
  });
};

const setClientFirstUseFalse = id => {
  models.Client.update({
    firstUse: false
  }, {
    where: { id }
  });
};

const setCaseFirstUseFalse = id => {
  models.Case.update({
    firstUse: false
  }, {
    where: { id }
  });
};

const setMismatch = async(mismatch, ProjectId, CaseId, ClientId) => {
  await models.RunResult.update({
    mismatch
  }, {
    where: { ProjectId, CaseId, ClientId }
  });
};

const getClients = async projectId => {
  return models.Client.findAll({
    where: { projectId },
    attributes: [['id', 'clientId'], 'browser', 'type', 'width', 'height', 'device', ['firstUse', 'clientFirstUse']]
  });
};

const getCases = async projectId => {
  return models.Case.findAll({
    where: { projectId },
    attributes: [['id', 'caseId'], 'url', ['firstUse', 'caseFirstUse']]
  });
};

const getScreenshotError = async({ projectId, caseId, clientId }) => {
  return models.RunResult.findAll({
    where: { projectId, caseId, clientId },
    attributes: ['screenshotError']
  });
};

const checkToken = async token => {
  return models.ProjectToken.find({
    where: { token },
    attributes: ['ProjectId']
  });
};

const getRunResult = async projectId => {
  return models.RunResult.findAll({
    where: { projectId },
    attributes: [
      'mismatch',
      'screenshotError',
      'lastErrorMessage',
      [Sequelize.col('Case.id'), 'caseId'],
      [Sequelize.col('Case.name'), 'caseName'],
      [Sequelize.col('Client.id'), 'clientId'],
      [Sequelize.col('Client.name'), 'clientName']
    ],
    includeIgnoreAttributes: false,
    include: [{
      model: models.Case,
      as: 'Case'
    }, {
      model: models.Client,
      as: 'Client'
    }]
  });
};

const getStatus = async ProjectId => {
  return models.Workflow.find({
    where: { ProjectId },
    attributes: ['status']
  });
};

const setFail = async ProjectId => {
  await models.Workflow.update({
    status: 'Fail'
  }, {
    where: { ProjectId }
  });
  await models.RunResult.update({
    mismatch: null
  }, {
    where: { ProjectId }
  });
};

const setDuration = async projectId => {
  let mismatch = await models.RunResult.findAll({
    where: {
      projectId,
      mismatch: { [Sequelize.Op.gt]: 0 }
    },
    attributes: ['mismatch']
  });
  let status = '';
  if (mismatch && mismatch.length > 0) {
    status = 'Need Validation';
  } else {
    status = 'Pass';
  }
  let startTime = await models.Workflow.find({
    where: { projectId },
    attributes: ['startTime']
  });
  let duration = new Date() - startTime.dataValues.startTime;
  models.Workflow.update({
    duration,
    status
  }, {
    where: { projectId }
  });
  return duration;
};

module.exports = {
  setWorkflow,
  setItemFirstUse,
  setClientFirstUseFalse,
  setCaseFirstUseFalse,
  setMismatch,
  setScreenshotError,
  clearScreenshotError,
  getClients,
  getCases,
  getScreenshotError,
  checkToken,
  getRunResult,
  setFail,
  setDuration,
  getStatus
};
