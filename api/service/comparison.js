const fs = require('fs');
const PNG = require('pngjs').PNG;
const path = require('path');
const pixelmatch = require('pixelmatch');
const {
  setMismatch,
  setDuration
} = require('./dbOperation');

const snapCompare = async item => {
  const fileName = `${item.projectId}/${item.caseId}.${item.clientId}`;
  let filesRead = 0;
  let mismatch;
  const doneReading = async() => {
    if (++filesRead < 2) return;
    const diff = new PNG({ width: img1.width, height: img1.height });
    mismatch = await pixelmatch(img1.data, img2.data, diff.data, img1.width, img1.height, { threshold: 0.1 });
    await diff.pack().pipe(fs.createWriteStream(path.join(__dirname, `../snapShots/difference/${fileName}.png`))).on('close', async() => {
      await setMismatch(mismatch, item.projectId, item.caseId, item.clientId);
      if (item.lastItem) {
        setDuration(item.projectId);
      }
    });
    return mismatch;
  };
  const img1 = fs.createReadStream(path.join(__dirname, `../snapShots/current/${fileName}.png`)).pipe(new PNG()).on('parsed', await doneReading);
  const img2 = fs.createReadStream(path.join(__dirname, `../snapShots/previous/${fileName}.png`)).pipe(new PNG()).on('parsed', await doneReading);
  await img2.on('close', () => {
  });
};

module.exports = {
  snapCompare
};
