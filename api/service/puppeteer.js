const axios = require('axios');
const chromeHost = process.env.CHROME_HOST || '172.30.0.50';
const chromePort = process.env.CHROME_PORT || 9222;
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const GalaxyS5 = devices['Galaxy S5'];
const Pixel2 = devices['Pixel 2'];
const Pixel2XL = devices['Pixel 2 XL'];
const iPhone5 = devices['iPhone 5'];
const iPhoneSE = devices['iPhone SE'];
const iPhone6 = devices['iPhone 6'];
const iPhone7 = devices['iPhone 7'];
const iPhone6Plus = devices['iPhone 6 Plus'];
const iPhoneX = devices['iPhone X'];
const iPad = devices['iPad'];
const iPadPro = devices['iPad Pro'];
const Nexus10 = devices['Nexus 10'];
const { snapCompare } = require('./comparison');
const { setDuration } = require('./dbOperation');

const clientDevices = {
  'Galaxy S5': GalaxyS5,
  'Pixel 2': Pixel2,
  'Pixel 2 XL': Pixel2XL,
  'iPhone 5': iPhone5,
  'iPhone SE': iPhoneSE,
  'iPhone 6': iPhone6,
  'iPhone 7': iPhone7,
  'iPhone 6 Plus': iPhone6Plus,
  'iPhone X': iPhoneX,
  'iPad': iPad,
  'iPad Pro': iPadPro,
  'Nexus 10': Nexus10
};

const puppeteerDoesSnapShot = async({ puppeteerUrl: url, browser: clientBrowser, path, type, width, height, device, item }) => {
  const chromeUrl = `http://${chromeHost}:${chromePort}/json/version`;
  const response = await axios.get(chromeUrl);
  const browserWSEndpoint = response.data.webSocketDebuggerUrl;
  const browser = await puppeteer.connect({
    browserWSEndpoint: browserWSEndpoint,
    ignoreHTTPSErrors: true
  });
  const page = await browser.newPage();
  if (type.localeCompare('viewport') === 0) {
    await page.setViewport({ width, height });
  } else {
    await page.emulate(clientDevices[device]);
  }

  await page.goto(url);
  await page.screenshot({
    fullPage: false,
    path: path
  });
  await page.close();
  await browser.disconnect();
  if (!item.firstUse) {
    await snapCompare(item);
  } else if (item.lastItem) {
    setDuration(item.projectId);
  }
};

module.exports = {
  puppeteerDoesSnapShot
};
