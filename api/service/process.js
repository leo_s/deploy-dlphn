const fs = require('fs');
const { puppeteerDoesSnapShot } = require('./puppeteer');
const {
  setItemFirstUse,
  setClientFirstUseFalse,
  setCaseFirstUseFalse,
  setScreenshotError,
  clearScreenshotError,
  getClients,
  getCases,
  getScreenshotError
} = require('./dbOperation');

const goToProcess = async projectId => {
  let clients = await getClients(projectId);
  let cases = await getCases(projectId);
  const buildPath = ['current', 'previous', 'difference'];
  buildPath.forEach(pathItem => {
    if (!fs.existsSync(`./snapShots/${pathItem}/${projectId}`)) {
      fs.mkdirSync(`./snapShots/${pathItem}/${projectId}`);
    }
  });
  const items = [];
  await clients.map(client => {
    cases.map(projectCase => {
      const { caseId, url, caseFirstUse } = projectCase.dataValues;
      const { clientId, browser, type, width, height, device, clientFirstUse } = client.dataValues;
      let path;
      let item;
      path = `./snapShots/current/${projectId}/${caseId}.${clientId}.png`;
      item = { firstUse: caseFirstUse || clientFirstUse, projectId, caseId, clientId };
      if (caseFirstUse || clientFirstUse) {
        setItemFirstUse(projectId, caseId, clientId, true);
      } else {
        setItemFirstUse(projectId, caseId, clientId, false);
      }
      let puppeteerUrl;
      if (url.includes('https://www.') || (url.includes('http://www.'))) {
        puppeteerUrl = url;
      } else if (url.includes('www.')) {
        puppeteerUrl = `https://${url}`;
      } else {
        puppeteerUrl = `https://www.${url}`;
      }
      items.push({ puppeteerUrl, browser, path, type, width, height, device, item });
    });
  });
  const timeoutForChromeErrorAction = timeout => new Promise(resolve => setTimeout(resolve, timeout));
  const SnapShot = async items => {
    for (let number = 0; number < items.length; number++) {
      const ScreenshotError = await getScreenshotError(items[number].item);
      items[number].item.firstUse = items[number].item.firstUse ||
        (ScreenshotError ? ScreenshotError[0].dataValues.screenshotError : true);
      if (items[number].item.firstUse === 1) {
        items[number].path = items[number].path.replace('current', 'previous');
      }
      if (items.length - 1 === number) {
        items[number].item.lastItem = true;
      }
      try {
        await puppeteerDoesSnapShot(items[number]);
        await clearScreenshotError(items[number].item);
      } catch (error) {
        await timeoutForChromeErrorAction(2000);
        await setScreenshotError(items[number].item, error);
      }
    }
  };
  clients.map(client => {
    setClientFirstUseFalse(client.dataValues.clientId);
  });
  cases.map(projectCase => {
    setCaseFirstUseFalse(projectCase.dataValues.caseId);
  });
  SnapShot(items);
};

module.exports = {
  goToProcess
};
