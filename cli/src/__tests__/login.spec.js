import React from 'react';
import LoginContainer from '../components/login/LoginContainer';
import renderer from 'react-test-renderer';
import reducers from "../components/reducers";
import {initialAuthState, initialProfileState} from "../initialState";
import reduxThunk from "redux-thunk";
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";

const store = createStore(reducers, {
  auth: initialAuthState,
  profile: initialProfileState
}, applyMiddleware(reduxThunk));

describe('Login test', () => {
  const props = {
    pristine:  false,
    submitting: false,
    handleSubmit: () => {},
  }

  it('renders correctly', () => {
    const SignupCont = renderer
      .create(
        <Provider store={store}>
          <LoginContainer {...props}/>
        </Provider>
      )
      .toJSON();
    expect(SignupCont).toMatchSnapshot();
  });
});
