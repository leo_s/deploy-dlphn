const localStorageService = require('./components/service/localStorage')

export const initialAuthState = {
  authTag: localStorageService.getAuth(),
  token: localStorage.getItem('token'),
  refreshToken: localStorage.getItem('refreshToken'),
  userEmail: ''
};

export const initialProfileState = {
  authTag: localStorageService.getAuth(),
  myId: localStorage.getItem('myId'),
};

export const initialClientState = {
  renderClient: 'showList',
  clients: [],
  initialValues: {},
  authTag: localStorageService.getAuth(),
  myId: localStorage.getItem('myId'),
};

export const initialCaseState = {
  renderCase: 'showList',
  cases: [],
  initialValues: {},
  authTag: localStorageService.getAuth(),
  myId: localStorage.getItem('myId'),
};

export const initialExclusionState = {
  renderExclusion: 'showList',
  exclusions: [],
  initialValues: {},
  authTag: localStorageService.getAuth(),
  myId: localStorage.getItem('myId'),
};

export const initialTokenState = {
  tokens: []
};

export const initialProjectState = {
  renderPage: 'showList',
  projects: [],
  error: '',
  initialValues: {},
  projectResult: [],
  projectNumber: 0,
  projectName: ''
};
