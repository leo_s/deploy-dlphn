const validate = (values) => {
  function isNotEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return true;
    }
    return false;
  }

  if (isNotEmpty(values)) {
    const errors = {}
    let requiredFields;
    if (values['type'] && values['type'].localeCompare('viewport') === 0) {
      requiredFields = [
        'name',
        'browser',
        'type',
        'width'
      ]
    }
    else {
      requiredFields = [
        'name',
        'browser',
        'type',
        'device'
      ]
    }
    requiredFields.forEach(field => {
      if (!values[field]) {
        errors[field] = 'Required'
      }
    })
    let width = parseInt(values['width'], 10);
    if (width < 300 || width > 1960 || isNaN(values['width'])) {
      errors.width = 'Width should be between 320 and 1960 (px)'
    }
    return errors
  }
}

module.exports = {
  validate
}
