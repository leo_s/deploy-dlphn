import React, {Component} from 'react';
import {connect} from 'react-redux';
import ClientList from './ClientList'
import ClientForm from './ClientForm'
import {
  postClientAction,
  editClientAction,
  showClientForm,
  getClientsAction,
  putClientAction,
  deleteClientAction,
} from '../actions/client';


class ClientContainer extends Component {

  onSubmitPostClient(values) {
    this.props.postClientAction(values)
  }

  onSubmitPutClient(values) {
    this.props.putClientAction(values)
  }

  onClickDeleteClient(values) {
    this.props.deleteClientAction(values)
  }

  onClickAddClient(){
    this.props.showClientForm()
  }

  onClickEditClient(selected){
    this.props.editClientAction(selected)
  }

  onClickGoToList(){
    this.props.getClientsAction()
  }

  componentDidMount() {
    this.props.getClientsAction()
  }

  render() {
    const {renderClient, error, clients=[], initialValues} = this.props.client;
    const {projectInitialValues} = this.props;
    let expose;
    if (renderClient.localeCompare('showList') === 0) {
      expose = <ClientList
        onClickAddClient={this.onClickAddClient.bind(this)}
        onClickEditClient={this.onClickEditClient.bind(this)}
        onClickDeleteClient={this.onClickDeleteClient.bind(this)}
        projectInitialValues={projectInitialValues}
        clients={clients}
      />;
    }
    else if (renderClient.localeCompare('showBlankForm') === 0) {
      expose = <ClientForm
        error={error}
        onSubmit={this.onSubmitPostClient.bind(this)}
        initialValues={{browser: 'Chrome'}}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />;
    }
    else if (renderClient.localeCompare('showForm') === 0) {
      expose = <ClientForm
        error={error}
        onSubmit={this.onSubmitPutClient.bind(this)}
        initialValues={initialValues}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />;
    }

    return (
      <div>
        {expose}
      </div>
    )
  }
}

function mapStateToProps({client}) {
  return {client};
}

export default connect(
  mapStateToProps,
  {
    postClientAction,
    editClientAction,
    getClientsAction,
    showClientForm,
    putClientAction,
    deleteClientAction,
  }
)(ClientContainer);
