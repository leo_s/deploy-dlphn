import React, {Component} from 'react';
import {connect} from 'react-redux';
import TokenList from './TokenList'
import { getTokensAction, postTokenAction, deleteTokenAction } from '../actions/token';


class TokenContainer extends Component {

  onClickDeleteToken(tokenId) {
    this.props.deleteTokenAction(tokenId)
  }

  onClickAddToken(){
    this.props.postTokenAction()
  }

  componentDidMount() {
    this.props.getTokensAction()
  }

  render() {
    const { tokens=[] } = this.props.token;
    const {projectInitialValues} = this.props;

    return (
      <div>
        <TokenList
          onClickAddToken={this.onClickAddToken.bind(this)}
          onClickDeleteToken={this.onClickDeleteToken.bind(this)}
          projectInitialValues={projectInitialValues}
          tokens={tokens}
        />
      </div>
    )
  }
}

function mapStateToProps({token}) {
  return {token};
}

export default connect(
  mapStateToProps,
  {
    getTokensAction,
    postTokenAction,
    deleteTokenAction
  }
)(TokenContainer);
