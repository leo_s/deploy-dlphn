import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import Signup from '../signup/SignupContainer'
import Login from '../login/LoginContainer'
import User from '../user/UserContainer'
import ListContainer from '../project/listSection/ListContainer'
import EditContainer from '../project/editSection/EditContainer'
import LaunchContainer from '../project/launchSection/LaunchContainer'

class Main extends Component {
  render() {
    return (
      <Switch>
        <Route path="/signup" component={props => <Signup {...props} />}/>
        <Route path="/login" component={props => <Login {...props} />}/>
        <Route path="/user" component={props => <User {...props} />}/>
        <Route path="/projectList" component={props => <ListContainer {...props} />}/>
        <Route path="/projectRelease" component={props => <EditContainer {...props} />}/>
        <Route path="/projectLaunch" component={props => <LaunchContainer {...props} />}/>
      </Switch>
    );
  }
}

export default Main;
