import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography/Typography";
import indigo from "@material-ui/core/colors/indigo";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import faker from "faker";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import HomeIcon from '@material-ui/icons/Home';
import WorkIcon from '@material-ui/icons/Work';
import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  title: {
    textAlign: 'center',
    marginBottom: 10,
    color: indigo[700],
  },
  bigAvatar: {
    width: 80,
    height: 80,
  },
  userName: {
    marginTop: 40,
  }
});

function Authorized(props) {
  const userData = [
    {item: 'New York, NY 10004', icon: <HomeIcon />},
    {item: 'Lineate', icon: <WorkIcon />},
    {item: props.userEmail, icon: <EmailIcon />},
    {item: '1-800-698-4637', icon: <PhoneIcon />}
  ]
  const {classes} = props;
  return (
    <div>
      <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
        User
      </Typography>
      <List className={classes.root}>
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt={'Gek Black'}
                    src={faker.image.avatar()}
                    className={classes.bigAvatar}
            />
          </ListItemAvatar>
          <ListItemText className={classes.userName}
                        secondary={'Gek Black'}
          />
        </ListItem>
        {userData.map((data, key) => {
          return(
            <div key={key}  id="page_user">
              <ListItem >
                <ListItemIcon>
                  {data.icon}
                </ListItemIcon>
                <ListItemText
                  primary={data.item}
                />
              </ListItem>
            </div>
          )}
        )}
      </List>
    </div>
  );
}

Authorized.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Authorized);
