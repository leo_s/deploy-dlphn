import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography/Typography";
import indigo from "@material-ui/core/colors/indigo";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import PersonIcon from '@material-ui/icons/Person';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  title: {
    textAlign: 'center',
    marginBottom: 10,
    color: indigo[700],
  },
  bigAvatar: {
    width: 80,
    height: 80,
  },
  userName: {
    marginTop: 40,
  }
});

function UnAuthorized(props) {
  const {classes} = props;
  return (
    <div>
      <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
        User
      </Typography>
      <List className={classes.root}>
        <ListItem>
          <ListItemIcon>
            <PersonIcon style={{ fontSize: 80 }}/>
          </ListItemIcon>
          <ListItemText
            primary='Unauthorized'
          />
        </ListItem>
      </List>
    </div>

  );
}

UnAuthorized.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UnAuthorized);
