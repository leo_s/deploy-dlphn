import React from 'react';
import PropTypes from 'prop-types';
import Authorized from './Authorized'
import UnAuthorized from './UnAuthorized'
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import indigo from "@material-ui/core/colors/indigo";
import grey from "@material-ui/core/colors/grey";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  title: {
    marginBottom: 10,
    color: indigo[700],
  },
  card: {
    margin: 40,
    background: grey[100],
  },
  user: {
    marginLeft: 30,
    marginRight: 30,
  },
  bigAvatar: {
    width: 80,
    height: 80,
  },
  userName: {
    marginTop: 40,
  }
});

function UserView(props) {
  const {classes} = props;
  let userContent;
  if (props.userEmail.localeCompare('Unauthorized') === 0) {
    userContent = <UnAuthorized/>;
  } else {
    userContent = <Authorized userEmail={props.userEmail}/>;
  }
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={5}>
        </Grid>
        <Grid item xs={4}>
          <Card className={classes.card}>
            <CardContent className={classes.user}>
              {userContent}
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={3}>
        </Grid>
      </Grid>
    </div>
  );
}

UserView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserView);
