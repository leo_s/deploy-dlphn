import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadUserProfileAction} from '../actions';
import User from './User'

class UserContainer extends Component {

  componentDidMount() {
    this.props.loadUserProfileAction()
      .catch((error) => {
        if (error.response.data.localeCompare('Unauthenticated') === 0) {
          this.props.history.push("/login")
        }
      })
  }

  render() {
    return (
      <User userEmail={this.props.userEmail ? this.props.userEmail : 'Unauthorized'}/>
    );
  }
}

function mapStateToProps({auth}) {
  return {
    userEmail: auth.userEmail,
  }
}

export default connect(
  mapStateToProps,
  {
    loadUserProfileAction,
  },
)(UserContainer)
