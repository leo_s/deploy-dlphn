import {
  GET_TOKENS,
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case GET_TOKENS:
      return {
        ...state, ...{
          tokens: action.payload,
        }
      }
    default:
      return {...state}
  }
}
