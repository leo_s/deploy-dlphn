import {
  GET_EXCLUSIONS,
  GET_EXCLUSION,
  SHOW_EXCLUSION_FORM,
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case SHOW_EXCLUSION_FORM:
      return {
        ...state, ...{
          renderExclusion: 'showBlankForm',
        }
      }
    case GET_EXCLUSIONS:
      return {
        ...state, ...{
          renderExclusion: 'showList',
          exclusions: action.payload,
        }
      }
    case GET_EXCLUSION:
      return {
        ...state, ...{
          renderExclusion: 'showForm',
          initialValues: action.payload,
        }
      }
    default:
      return {...state}
  }
}
