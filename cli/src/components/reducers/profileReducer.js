import {
  SIGN_UP,
  GET_AUTH
} from '../actions/types';

const localStorageService = require('../service/localStorage')

export default function (state = {}, action) {
  switch (action.type) {
    case SIGN_UP:
      localStorageService.login(action.payload)
      return {...state, ...{authTag: true}}
    case GET_AUTH:
      return {...state, ...{authTag: localStorageService.getAuth()}}
    default:
      return state;
  }
}
