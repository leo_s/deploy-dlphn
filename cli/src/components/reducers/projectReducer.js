import {
  SHOW_PROJECT_NAME_FORM,
  SHOW_PROJECT_LIST,
  GET_PROJECTS,
  GET_PROJECT,
  GET_PROJECT_NAME,
  GET_PROJECT_RESULT,
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case GET_PROJECTS:
      return {
        ...state, ...{
          renderPage: 'showList',
          projects: action.payload,
        }
      };
    case SHOW_PROJECT_NAME_FORM:
        return {
          ...state, ...{
            renderPage: 'showForm',
            error: ''
          }
      };
    case SHOW_PROJECT_LIST:
      return {
        ...state, ...{
          renderPage: 'showList',
          error: ''
        }
      };
    case GET_PROJECT:
      return {
        ...state, ...{
          initialValues: action.payload,
        }
      };
    case GET_PROJECT_RESULT:
      return {
        ...state, ...{
          projectResult: action.payload,
        }
      };
    case GET_PROJECT_NAME:
      return {
        ...state, ...{
          renderPage: 'showDescription',
          initialValues: action.payload
        }
      };
    default:
      return {...state}
  }
}
