import {
  GET_CLIENTS,
  GET_CLIENT,
  SHOW_CLIENT_FORM,
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case SHOW_CLIENT_FORM:
      return {
        ...state, ...{
          renderClient: 'showBlankForm',
        }
      }
    case GET_CLIENTS:
      return {
        ...state, ...{
          renderClient: 'showList',
          clients: action.payload,
        }
      }
    case GET_CLIENT:
      return {
        ...state, ...{
          renderClient: 'showForm',
          initialValues: action.payload,
        }
      }
    default:
      return {...state}
  }
}
