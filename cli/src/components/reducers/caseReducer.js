import {
  GET_CASES,
  GET_CASE,
  SHOW_CASE_FORM,
  SHOW_CASE_NAME_FORM,
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case SHOW_CASE_FORM:
      return {
        ...state, ...{
          renderCase: 'showBlankForm',
        }
      }
    case GET_CASES:
      return {
        ...state, ...{
          renderCase: 'showList',
          cases: action.payload,
        }
      }
    case GET_CASE:
      return {
        ...state, ...{
          renderCase: 'showForm',
          initialValues: action.payload,
        }
      }
    case SHOW_CASE_NAME_FORM:
      return {
        ...state, ...{
          renderPage: 'showForm',
          error: ''
        }
      }
    default:
      return {...state}
  }
}
