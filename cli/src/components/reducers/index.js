import {combineReducers} from 'redux';
import {reducer as reduxForm} from 'redux-form';
import authReducer from './authReducer';
import profileReducer from './profileReducer';
import projectReducer from './projectReducer';
import clientReducer from './clientReducer';
import caseReducer from './caseReducer';
import exclusionReducer from './exclusionReducer';
import tokenReducer from './tokenReducer';

export default combineReducers({
  auth: authReducer,
  profile: profileReducer,
  project: projectReducer,
  client: clientReducer,
  projectCase: caseReducer,
  projectExclusion: exclusionReducer,
  token: tokenReducer,
  form: reduxForm,
});
