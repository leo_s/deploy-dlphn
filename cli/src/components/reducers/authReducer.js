import {
  LOGIN,
  LOGOUT,
  USER,
} from '../actions/types';

const localStorageService = require('../service/localStorage')

export default function (state = {}, action) {
  switch (action.type) {
    case LOGIN:
      localStorageService.login(action.payload)
      return {...state, ...{authTag: true}}
    case LOGOUT:
      localStorageService.logout()
      return {
        ...state, ...{
          authTag: false,
          userEmail: '',
        }
      }
    case USER:
      return {
        ...state, ...{
          userEmail: action.payload,
          authTag: localStorageService.getAuth(),
        }
      }
    default:
      return {...state, ...{authTag: localStorageService.getAuth()}}
  }
}
