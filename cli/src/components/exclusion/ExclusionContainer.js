import React, {Component} from 'react';
import {connect} from 'react-redux';
import ExclusionList from './ExclusionList'
import ExclusionForm from './ExclusionForm'
import {
  postExclusionAction,
  editExclusionAction,
  showExclusionForm,
  getExclusionsAction,
  putExclusionAction,
  deleteExclusionAction,
} from '../actions/exclusion';


class ExclusionContainer extends Component {

  onSubmitPostExclusion(values) {
    this.props.postExclusionAction(values)
  }

  onSubmitPutExclusion(values) {
    this.props.putExclusionAction(values)
  }

  onClickDeleteExclusion(values) {
    this.props.deleteExclusionAction(values)
  }

  onClickAddExclusion(){
    this.props.showExclusionForm()
  }

  onClickEditExclusion(selected){
    this.props.editExclusionAction(selected)
  }

  onClickGoToList(){
    this.props.getExclusionsAction()
  }

  componentDidMount() {
    this.props.getExclusionsAction()
  }

  render() {
    const {renderExclusion, error, exclusions=[], initialValues} = this.props.projectExclusion;
    const {projectInitialValues} = this.props;
    let expose;
    if (renderExclusion.localeCompare('showList') === 0) {
      expose = <ExclusionList
        onClickAddExclusion={this.onClickAddExclusion.bind(this)}
        onClickEditExclusion={this.onClickEditExclusion.bind(this)}
        onClickDeleteExclusion={this.onClickDeleteExclusion.bind(this)}
        projectInitialValues={projectInitialValues}
        exclusions={exclusions}
      />;
    }
    else if (renderExclusion.localeCompare('showBlankForm') === 0) {
      expose = <ExclusionForm
        error={error}
        onSubmit={this.onSubmitPostExclusion.bind(this)}
        initialValues={{browser: 'Chrome'}}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />;
    }
    else if (renderExclusion.localeCompare('showForm') === 0) {
      expose = <ExclusionForm
        error={error}
        onSubmit={this.onSubmitPutExclusion.bind(this)}
        initialValues={initialValues}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />;
    }

    return (
      <div>
        {expose}
      </div>
    )
  }
}

function mapStateToProps({projectExclusion}) {
  return {projectExclusion};
}

export default connect(
  mapStateToProps,
  {
    postExclusionAction,
    editExclusionAction,
    getExclusionsAction,
    showExclusionForm,
    putExclusionAction,
    deleteExclusionAction,
  }
)(ExclusionContainer);
