const validate = (values) => {
  function isNotEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return true;
    }
    return false;
  }

  if (isNotEmpty(values)) {
    const errors = {}
    const requiredFields = [
      'exclusion',
    ]

    requiredFields.forEach(field => {
      if (!values[field]) {
        errors[field] = 'Required'
      }
    })
    return errors
  }
}

module.exports = {
  validate
}
