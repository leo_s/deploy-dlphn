import {
  GET_TOKENS,
} from "./types";

const apiService = require('../service/apiService')
const localStorageService = require('../service/localStorage')

export function getTokensAction () {
  let projectNumber = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProjectTokens(projectNumber, config)
      .then((response) => {
          dispatch({
            type: GET_TOKENS,
            payload: response.data
          })
        }
      )
  }
}

export function postTokenAction () {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.postProjectToken({ProjectId}, config)
      .then((response) => {
        dispatch(getTokensAction()
        )}
      )
  }
}

export function deleteTokenAction (tokenId) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.deleteProjectToken(tokenId[0], config)
      .then((response) => {
        dispatch(getTokensAction()
        )}
      )
  }
}

