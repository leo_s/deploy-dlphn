import {
  GET_CLIENTS,
  GET_CLIENT,
  SHOW_CLIENT_FORM,
  ERROR_ADD_CLIENT,
} from "./types";

const apiService = require('../service/apiService')
const localStorageService = require('../service/localStorage')

export function showClientForm() {
  return function (dispatch) {
    dispatch({
      type: SHOW_CLIENT_FORM,
    })
  }
}

export function postClientAction(values, redirect) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.postClient({...values, ...{ProjectId}}, config)
      .then((response) => {
        apiService.getClients(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_CLIENTS,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_CLIENT,
          payload: error.response.data
        })
      })
  }
}

export function getClientsAction () {
  let clientNumber = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getClients(clientNumber, config)
      .then((response) => {
          dispatch({
            type: GET_CLIENTS,
            payload: response.data
          })
        }
      )
  }
}

export function editClientAction(clientNumber) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getClient(clientNumber, config)
      .then((response) => {
          dispatch({
            type: GET_CLIENT,
            payload: response.data
          })
        }
      )
  }
}


export function putClientAction(values) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.putClient(values, config)
      .then((response) => {
        apiService.getClients(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_CLIENTS,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_CLIENT,
          payload: error.response.data
        })
      })
  }
}

export function deleteClientAction(values) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.deleteClient(values[0], config)
      .then((response) => {
        apiService.getClients(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_CLIENTS,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_CLIENT,
          payload: error.response.data
        })
      })
  }
}
