import {
  GET_EXCLUSIONS,
  GET_EXCLUSION,
  SHOW_EXCLUSION_FORM,
  ERROR_ADD_EXCLUSION,
} from "./types";

const apiService = require('../service/apiService')
const localStorageService = require('../service/localStorage')

export function showExclusionForm() {
  return function (dispatch) {
    dispatch({
      type: SHOW_EXCLUSION_FORM,
    })
  }
}

export function postExclusionAction(values, redirect) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.postExclusion({...values, ...{ProjectId}}, config)
      .then((response) => {
        apiService.getExclusions(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_EXCLUSIONS,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_EXCLUSION,
          payload: error.response.data
        })
      })
  }
}

export function getExclusionsAction () {
  let exclusionNumber = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getExclusions(exclusionNumber, config)
      .then((response) => {
          dispatch({
            type: GET_EXCLUSIONS,
            payload: response.data
          })
        }
      )
  }
}

export function editExclusionAction(exclusionNumber) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getExclusion(exclusionNumber, config)
      .then((response) => {
          dispatch({
            type: GET_EXCLUSION,
            payload: response.data
          })
        }
      )
  }
}


export function putExclusionAction(values) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.putExclusion(values, config)
      .then((response) => {
        apiService.getExclusions(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_EXCLUSIONS,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_EXCLUSION,
          payload: error.response.data
        })
      })
  }
}

export function deleteExclusionAction(values) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.deleteExclusion(values[0], config)
      .then((response) => {
        apiService.getExclusions(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_EXCLUSIONS,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_EXCLUSION,
          payload: error.response.data
        })
      })
  }
}
