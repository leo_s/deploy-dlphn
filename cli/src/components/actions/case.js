import {
  GET_CASES,
  GET_CASE,
  SHOW_CASE_FORM,
  ERROR_ADD_CASE,
  GET_PROJECT_NAME,
  SHOW_CASE_NAME_FORM,
} from "./types";

const apiService = require('../service/apiService')
const localStorageService = require('../service/localStorage')

export function showCaseForm() {
  return function (dispatch) {
    dispatch({
      type: SHOW_CASE_FORM,
    })
  }
}

export function postCaseAction(values, redirect) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.postCase({...values, ...{ProjectId}}, config)
      .then((response) => {
        apiService.getCases(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_CASES,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_CASE,
          payload: error.response.data
        })
      })
  }
}

export function getCasesAction () {
  let caseNumber = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getCases(caseNumber, config)
      .then((response) => {
          dispatch({
            type: GET_CASES,
            payload: response.data
          })
        }
      )
  }
}

export function editCaseAction(caseNumber) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getCase(caseNumber, config)
      .then((response) => {
          dispatch({
            type: GET_CASE,
            payload: response.data
          })
        }
      )
  }
}


export function putCaseAction(values) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.putCase(values, config)
      .then((response) => {
        apiService.getCases(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_CASES,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_CASE,
          payload: error.response.data
        })
      })
  }
}

export function deleteCaseAction(values) {
  let ProjectId = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.deleteCase(values[0], config)
      .then((response) => {
        apiService.getCases(ProjectId, config)
          .then((response) => {
            dispatch({
              type: GET_CASES,
              payload: response.data
            })
          })
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_CASE,
          payload: error.response.data
        })
      })
  }
}

export function editSectionRedirectionAction(selected, redirect) {
  return function (dispatch) {
    localStorageService.serProjectNumber(selected);
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProject(selected, config)
      .then((response) => {
          dispatch({
            type: GET_PROJECT_NAME,
            payload: response.data
          })
          redirect("/projectRelease")
        }
      )
  }
}

export function showCaseNameForm() {
  return function (dispatch) {
    dispatch({
      type: SHOW_CASE_NAME_FORM,
    })
  }
}

