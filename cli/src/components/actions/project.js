import {
  SHOW_PROJECT_NAME_FORM,
  SHOW_PROJECT_LIST,
  ERROR_ADD_PROJECT,
  GET_PROJECTS,
  GET_PROJECT,
  GET_PROJECT_NAME,
  GET_PROJECT_RESULT,
} from "./types";

const apiService = require('../service/apiService')
const localStorageService = require('../service/localStorage')

export function getProjectsAction() {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProjects(config)
      .then((response) => {
          dispatch({
            type: GET_PROJECTS,
            payload: response.data
          })
        }
      )
  }
}

export function getProjectAction() {
  let projectNumber = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProject(projectNumber, config)
      .then((response) => {
          dispatch({
            type: GET_PROJECT,
            payload: response.data
          })
        }
      )
  }
}

export function getProjectResultAction() {
  let projectNumber = localStorageService.getProjectNumber();
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProjectResul(projectNumber, config)
      .then((response) => {
          dispatch({
            type: GET_PROJECT_RESULT,
            payload: response.data
          })
        }
      )
  }
}

export function showProjectNameForm() {
  return function (dispatch) {
    dispatch({
      type: SHOW_PROJECT_NAME_FORM,
    })
  }
}

export function postProjectAction(values, redirect) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.postProject(values, config)
      .then((response) => {
        dispatch({
          type: SHOW_PROJECT_LIST,
        })
        redirect("/projectList")
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_PROJECT,
          payload: error.response.data
        })
        redirect("/projectList")
      })
  }
}

export function deleteProjectAction(values, redirect) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.deleteProject(values[0], config)
      .then((response) => {
        dispatch({
          type: SHOW_PROJECT_LIST,
        })
        redirect("/projectList")
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_PROJECT,
          payload: error.response.data
        })
        redirect("/projectList")
      })
  }
}

export function putProjectAction(values, redirect) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.putProject(values, config)
      .then((response) => {
        dispatch({
          type: SHOW_PROJECT_LIST,
        })
        redirect("/projectList")
      })
      .catch((error) => {
        dispatch({
          type: ERROR_ADD_PROJECT,
          payload: error.response.data
        })
        redirect("/projectList")
      })
  }
}

export function editSectionRedirectionAction(selected, redirect) {
  return function (dispatch) {
    localStorageService.serProjectNumber(selected);
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProject(selected, config)
      .then((response) => {
          dispatch({
            type: GET_PROJECT_NAME,
            payload: response.data
          })
          redirect("/projectRelease")
        }
      )
  }
}

export function launchSectionRedirectionAction(selected, redirect) {
  return function (dispatch) {
    localStorageService.serProjectNumber(selected);
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.getProject(selected, config)
      .then((response) => {
          dispatch({
            type: GET_PROJECT_NAME,
            payload: response.data
          })
          redirect("/projectLaunch")
        }
      )
  }
}

export function startProjectAction(id, redirect) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.executeProject(id, config)
      .then((response) => {
        dispatch({
          type: SHOW_PROJECT_LIST,
        })
        redirect("/projectList")
      })
  }
}

export function approveNewSnapshotAction(projectId, caseId, clientId, redirect) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.approveSnapshot(projectId, caseId, clientId, config)
      .then(() => {
        dispatch(
          getProjectResultAction()
        )
        redirect("/projectLaunch")
      })
  }
}

export function failProjectAction(projectId, redirect) {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.failProject({projectId}, config)
      .then(() => {
        dispatch(
          getProjectsAction()
        )
        redirect("/projectList")
      })
  }
}
