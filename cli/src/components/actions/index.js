import {
  LOGIN,
  LOGOUT,
  USER,
} from './types';

const apiService = require('../service/apiService')
const localStorageService = require('../service/localStorage')

export function loginAction(values, redirect) {
  return function (dispatch) {
    return apiService.login(values)
      .then((response) => {
          dispatch({
            type: LOGIN,
            payload: response.data
          })
          redirect("/user")
        }
      )
      .catch((error) => {
        redirect("/login")
      })
  }
}

export function logoutAction(redirect) {
  return function (dispatch) {
    redirect("/login")
    dispatch({
      type: LOGOUT,
    })
  }
}

export function loadUserProfileAction() {
  return function (dispatch) {
    const token = localStorageService.getToken();
    const config = {
      headers: {'Authorization': `Bearer ${token}`}
    };
    return apiService.user(config)
      .then((response) => {
          dispatch({
            type: USER,
            payload: response.data
          })
        }
      )
  }
}
