import {SIGN_UP, GET_AUTH} from "./types";

const apiService = require('../service/apiService')

export function signUpAction(values, redirect) {
  return function (dispatch) {
    return apiService.signUp(values)
      .then((response) => {
        dispatch({
          type: SIGN_UP,
          payload: response.data
        })
        redirect("/user")
      })
      .catch((error) => {
        redirect("/signup")
      })
  }
}

export function getAuth() {
  return function (dispatch) {
    dispatch({
      type: GET_AUTH,
    })
  }
}



