import React, {Component} from 'react';
import {connect} from 'react-redux';
import CaseList from './CaseList'
import CaseForm from './CaseForm'
import {
  postCaseAction,
  editCaseAction,
  showCaseForm,
  getCasesAction,
  putCaseAction,
  deleteCaseAction,
} from '../actions/case';


class CaseContainer extends Component {

  onSubmitPostCase(values) {
    this.props.postCaseAction(values)
  }

  onSubmitPutCase(values) {
    this.props.putCaseAction(values)
  }

  onClickDeleteCase(values) {
    this.props.deleteCaseAction(values)
  }

  onClickAddCase(){
    this.props.showCaseForm()
  }

  onClickEditCase(selected){
    this.props.editCaseAction(selected)
  }

  onClickGoToList(){
    this.props.getCasesAction()
  }

  componentDidMount() {
    this.props.getCasesAction()
  }

  render() {
    const {renderCase, error, cases=[], initialValues} = this.props.projectCase;
    const {projectInitialValues} = this.props;
    let expose;
    if (renderCase.localeCompare('showList') === 0) {
      expose = <CaseList
        onClickAddCase={this.onClickAddCase.bind(this)}
        onClickEditCase={this.onClickEditCase.bind(this)}
        onClickDeleteCase={this.onClickDeleteCase.bind(this)}
        projectInitialValues={projectInitialValues}
        cases={cases}
      />;
    }
    else if (renderCase.localeCompare('showBlankForm') === 0) {
      expose = <CaseForm
        error={error}
        onSubmit={this.onSubmitPostCase.bind(this)}
        initialValues={{browser: 'Chrome'}}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />;
    }
    else if (renderCase.localeCompare('showForm') === 0) {
      expose = <CaseForm
        error={error}
        onSubmit={this.onSubmitPutCase.bind(this)}
        initialValues={initialValues}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />;
    }

    return (
      <div>
        {expose}
      </div>
    )
  }
}

function mapStateToProps({projectCase}) {
  return {projectCase};
}

export default connect(
  mapStateToProps,
  {
    postCaseAction,
    editCaseAction,
    getCasesAction,
    showCaseForm,
    putCaseAction,
    deleteCaseAction,
  }
)(CaseContainer);
