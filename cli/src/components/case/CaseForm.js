import React from 'react';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Grid from "@material-ui/core/Grid/Grid";
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import indigo from "@material-ui/core/colors/indigo";
import red from "@material-ui/core/colors/red";
import grey from "@material-ui/core/colors/grey";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import connect from "react-redux/es/connect/connect";

const {validate} = require('./Validate');

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  title: {
    marginBottom: -10,
    color: indigo[700],
  },
  error: {
    marginTop: 10,
    marginBottom: -10,
    color: red[500],
  },
  card: {
    margin: 40,
    background: grey[100],
  },
  form: {
    marginLeft: 70,
    marginRight: 70,
  },
  button: {
    marginTop: 15,
    marginLeft: 25,
  },

});

const renderTextField = ({
                           label,
                           input,
                           meta: {touched, invalid, error},
                           ...custom
                         }) => {
  return(
    <FormControl fullWidth={true}>
      <TextField
        label={label}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        margin="normal"
        {...input}
        {...custom}
      />
    </FormControl>
  )};

const renderFromHelper = ({ touched, error }) => {
  if (!(touched && error)) {
    return
  } else {
    return <FormHelperText>{touched && error}</FormHelperText>
  }
};

const renderSelectField = ({
                             input,
                             label,
                             meta: { touched, error },
                             children,
                             ...custom
                           }) => (
  <FormControl fullWidth={true} error={touched && error}>
    <InputLabel
      className='classes.card'
      htmlFor="age-native-simple">{label}
    </InputLabel>
    <Select
      autoWidth={true}
      native
      {...input}
      {...custom}
      inputProps={{
        name: 'browser',
        id: 'age-native-simple'
      }}
    >
      {children}
    </Select>
    {renderFromHelper({ touched, error })}
  </FormControl>)

function CaseConditionForm(props) {
  const {classes, typeValue, onClickGoToList} = props;
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={2}>
        </Grid>
        <Grid item xs={9}>
          <Card className={classes.card}>
            <CardContent className={classes.form}>
              <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                Case
              </Typography>
              {props.error && <Typography gutterBottom variant="subtitle2" component="h2" className={classes.error}>
                ERROR: {props.error}
              </Typography>}

              <form onSubmit={props.handleSubmit(props.onSubmit)} id="form_login">
                <div>
                  <Field
                    name="name"
                    component={renderTextField}
                    label="Provide the Case name"/>
                </div>
                <div>
                  <Field
                    name="description"
                    component={renderTextField}
                    label="Provide the Case description"/>
                </div>
                <div>
                  <Field
                    name="url"
                    component={renderTextField}
                    label="Provide the Case url"/>
                </div>
                <div className={classes.button}>
                  <Button type="submit" variant="contained" color="primary" name="form__button_submit"
                          disabled={props.pristine || props.submitting} className={classes.button}>
                    Submit
                  </Button>
                  <Button variant="contained" color="secondary" name="form__button_submit"
                          onClick={onClickGoToList} className={classes.button}>
                    Go back
                  </Button>
                </div>
              </form>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={1}>
        </Grid>
      </Grid>
    </div>
  )
}

CaseConditionForm = (reduxForm({
  form: 'CaseFormName',
  validate
})(CaseConditionForm));


const selector = formValueSelector('CaseFormName');
CaseConditionForm = connect(state => {
  const typeValue = selector(state, 'type');
  return {
    typeValue,
  }
})(CaseConditionForm);

export default withStyles(styles)(CaseConditionForm)
