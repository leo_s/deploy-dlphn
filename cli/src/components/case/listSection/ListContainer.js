import React, {Component} from 'react';
import {connect} from 'react-redux';
import ListControl from './ListControl'
import {
  showCaseNameForm,
  postCaseAction,
  getCasesAction,
  editSectionRedirectionAction,
  deleteCaseAction,
} from '../../actions/case';
import CaseForm from "../CaseForm";


class CaseContainer extends Component {

  onClickPostCase(values) {
    this.props.postCaseAction(values, (path) => {
      this.props.history.push(path)
    })
  }

  onClickAddCase(){
    this.props.showCaseNameForm()
  }
  onClickDeleteCase(selected){
    this.props.deleteCaseAction(selected, (path) => {
      this.props.history.push(path)
    })
  }
  onClickEditCase(selected){
    this.props.editSectionRedirectionAction(selected, (path) => {
      this.props.history.push(path)
    })
  }

  onClickGoToList(){
    this.props.getCasesAction()
  }

  componentDidMount() {
    this.props.getCasesAction()
  }

  render() {
    const {renderPage, cases} = this.props.projectCase;
    const {projectInitialValues} = this.props;
    return (
      <ListControl
        cases={cases}
        onClickAddCase={this.onClickAddCase.bind(this)}
        onClickEditCase={this.onClickEditCase.bind(this)}
        renderPage={renderPage}
        onClickPostCase={this.onClickPostCase.bind(this)}
        onClickDeleteCase={this.onClickDeleteCase.bind(this)}
        projectInitialValues={projectInitialValues}
        onClickGoToList={this.onClickGoToList.bind(this)}
      />
    )
  }
}

function mapStateToProps({projectCase}) {
  return {projectCase};
}

export default connect(
  mapStateToProps,
  {
    showCaseNameForm,
    postCaseAction,
    getCasesAction,
    editSectionRedirectionAction,
    deleteCaseAction,
  }
)(CaseContainer);
