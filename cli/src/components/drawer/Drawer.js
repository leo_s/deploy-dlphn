import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import {Link} from 'react-router-dom'
import {MenuList, MenuItem} from '@material-ui/core';
import {loginAction, logoutAction} from "../actions";
import {withRouter} from 'react-router';
import indigo from "@material-ui/core/colors/indigo";
import pink from "@material-ui/core/colors/pink";
import PersonAdd from '@material-ui/icons/PersonAdd';
import PersonOutline from '@material-ui/icons/PersonOutline';
import Person from '@material-ui/icons/Person';
import PersonPin from '@material-ui/icons/PersonPin';
import List from '@material-ui/icons/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: indigo[50],
  },
  toolbar: {
    height: 50,
    padding: 15,
    textAlign: 'center',
    color: pink[700],
    backgroundColor: indigo[50],
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
});

class PermanentDrawerLeft extends Component {
  render() {
    const {classes, logoutAction, history} = this.props;
    const login = this.props.auth.authTag;
    switch (login) {
      case true:
        return (
          <div className={classes.root}>
            <CssBaseline/>
            <Drawer
              className={classes.drawer}
              variant="permanent"
              classes={{
                paper: classes.drawerPaper,
              }}
              anchor="left"
            >
              <div className={classes.toolbar}>
                Snap-checker service
              </div>
              <Divider/>
              <div>
                <MenuList>
                  <MenuItem onClick={() => logoutAction(() => {
                    history.push("/login")
                  })}>
                    <ListItemIcon className={classes.icon}>
                      <PersonOutline />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Logout" />
                  </MenuItem>
                  <MenuItem component={Link} to="/user">
                    <ListItemIcon className={classes.icon}>
                      <PersonPin />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="User" />
                  </MenuItem>
                  <MenuItem component={Link} to="/projectList">
                    <ListItemIcon className={classes.icon}>
                      <List />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Project" />
                  </MenuItem>
                </MenuList>
              </div>
            </Drawer>
          </div>
        );
      default:
        return (
          <div className={classes.root}>
            <CssBaseline/>
            <Drawer
              className={classes.drawer}
              variant="permanent"
              classes={{
                paper: classes.drawerPaper,
              }}
              anchor="left"
            >
              <div className={classes.toolbar}>
                Snap-checker service
              </div>
              <Divider/>
              <div>
                <MenuList>
                  <MenuItem component={Link} to="/signup">
                    <ListItemIcon className={classes.icon}>
                      <PersonAdd />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Signup" />
                  </MenuItem>
                  <MenuItem component={Link} to="/login">
                    <ListItemIcon className={classes.icon}>
                      <Person />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Login" />
                  </MenuItem>
                  <MenuItem component={Link} to="/user">
                    <ListItemIcon className={classes.icon}>
                      <PersonPin />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="User" />
                  </MenuItem>
                </MenuList>
              </div>
            </Drawer>
          </div>
        )
    }
  }
}

PermanentDrawerLeft.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps({auth}) {
  return {auth};
}

export default withRouter(connect(
  mapStateToProps,
  {loginAction, logoutAction}
)(withStyles(styles)(PermanentDrawerLeft)));
