import axios from 'axios';
import {GET_PREVIOUS_SNAPSHOT} from "../actions/types";

export const login = async values => {
  return await axios
    .post(`api/v1/login`, values);
};

export const signUp = async values => {
  return await axios
    .post(`api/v1/signUp`, values)
};

export const user = async config => {
  return await axios
    .get(`api/v1/user`, config)
};

export const postProject = async (values, config) => {
  return await axios
    .post(`api/v1/project`, values, config);
};

export const deleteProject = async (projectNumber, config) => {
  return await axios
    .delete(`api/v1/project/${projectNumber}`, config);
};

export const getProject = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/project/${projectNumber}`, config)
};

export const getProjectResul = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/result/${projectNumber}`, config)
};

export const getProjects = async config => {
  return await axios
    .get(`api/v1/projects`, config)
};

export const putProject = async (values, config) => {
  return await axios
    .put(`api/v1/project`, values, config);
};

export const postClient = async (values, config) => {
  return await axios
    .post(`api/v1/client`, values, config);
};

export const getClients = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/clients/${projectNumber}`, config)
};

export const getClient = async (clientNumber, config) => {
  return await axios
    .get(`api/v1/client/${clientNumber}`, config)
};

export const putClient = async (values, config) => {
  return await axios
    .put(`api/v1/client`, values, config);
};

export const deleteClient = async (clientNumber, config) => {
  return await axios
    .delete(`api/v1/client/${clientNumber}`, config);
};

export const postCase = async (values, config) => {
  return await axios
    .post(`api/v1/case`, values, config);
};

export const getCases = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/cases/${projectNumber}`, config)
};

export const getCase = async (caseNumber, config) => {
  return await axios
    .get(`api/v1/case/${caseNumber}`, config)
};

export const putCase = async (values, config) => {
  return await axios
    .put(`api/v1/case`, values, config);
};

export const deleteCase = async (caseNumber, config) => {
  return await axios
    .delete(`api/v1/case/${caseNumber}`, config);
};

export const postExclusion = async (values, config) => {
  return await axios
    .post(`api/v1/projectExclusion`, values, config);
};

export const getExclusions = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/projectExclusions/${projectNumber}`, config)
};

export const getExclusion = async (exclusionNumber, config) => {
  return await axios
    .get(`api/v1/projectExclusion/${exclusionNumber}`, config)
};

export const putExclusion = async (values, config) => {
  return await axios
    .put(`api/v1/projectExclusion`, values, config);
};

export const deleteExclusion = async (exclusionNumber, config) => {
  return await axios
    .delete(`api/v1/projectExclusion/${exclusionNumber}`, config);
};

export const executeProject = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/execute/${projectNumber}`, config)
};

export const approveSnapshot = async (projectId, caseId, clientId, config) => {
  return await axios
    .get(`api/v1/approve/${projectId}/${caseId}/${clientId}`, config)
};

export const getProjectTokens = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/projectTokens/${projectNumber}`, config)
};

export const postProjectToken = async (values, config) => {
  return await axios
    .post(`api/v1/projectToken`, values, config)
};

export const deleteProjectToken = async (tokenId, config) => {
  return await axios
    .delete(`api/v1/projectToken/${tokenId}`, config)
};

export const failProject = async (values, config) => {
  return await axios
    .put(`api/v1/fail`, values, config);
};

