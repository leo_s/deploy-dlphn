import axios from "axios";

export const postExclusion = async (values, config) => {
  return await axios
    .post(`api/v1/exclusion`, values, config);
};

export const getExclusions = async (projectNumber, config) => {
  return await axios
    .get(`api/v1/exclusions/${projectNumber}`, config)
};

export const getExclusion = async (exclusionNumber, config) => {
  return await axios
    .get(`api/v1/exclusion/${exclusionNumber}`, config)
};

export const putExclusion = async (values, config) => {
  return await axios
    .put(`api/v1/exclusion`, values, config);
};

export const deleteExclusion = async (exclusionNumber, config) => {
  return await axios
    .delete(`api/v1/exclusion/${exclusionNumber}`, config);
};
