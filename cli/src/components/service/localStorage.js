const login = (payload) => {
  localStorage.setItem('isAuth', true);
  localStorage.setItem('token', payload.token);
  localStorage.setItem('refreshToken', payload.refreshToken);
}

const logout = () => {
  localStorage.setItem('isAuth', false);
  localStorage.setItem('token', '');
  localStorage.setItem('refreshToken', '');
}

const getAuth = () => {
  const isAuth = localStorage.getItem('isAuth')
  if (isAuth && isAuth.localeCompare('true') === 0) {
    return true
  }
  return false
}

const getToken = () => localStorage.getItem('token');

const serProjectNumber = (projectNumber) => {
  localStorage.setItem('projectNumber', projectNumber);
};

const getProjectNumber = () => localStorage.getItem('projectNumber');

module.exports = {
  login,
  logout,
  getAuth,
  getToken,
  serProjectNumber,
  getProjectNumber,
}
