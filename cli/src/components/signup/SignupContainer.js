import React, {Component} from 'react';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {signUpAction} from '../actions/profile';
import Signup from './Signup'

const {validate} = require('./Validate');

class SignUpContainer extends Component {

  onSubmit(values) {
    this.props.signUpAction(values, (path) => {
      this.props.history.push(path)
    })
  }

  render() {
    return (
      <Signup
        onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}
        pristine={this.props.pristine}
        submitting={this.props.submitting}
      />
    )
  }
}

export default connect(
  null,
  {signUpAction}
)(reduxForm({
  form: 'SignUpContainer',
  validate
})(SignUpContainer));
