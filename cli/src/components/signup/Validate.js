const validate = (values) => {
  function isNotEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return true;
    }
    return false;
  }

  if (isNotEmpty(values)) {
    const errors = {}
    const passPattern = /(?=^.{6,10}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/;
    const requiredFields = [
      'name',
      'email',
      'password',
      'repPassword'
    ]
    requiredFields.forEach(field => {
      if (!values[field]) {
        errors[field] = 'Required'
      }
    })
    if (
      values.email &&
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = 'Invalid email address'
    }
    if (passPattern.test(values.password) === false) {
      errors.password = 'Password should be at 6-10 characters, contain at least one number, one lowercase letter, one uppercase letter, one special character'
    }
    if (`${values.password}`.localeCompare(values.repPassword) !== 0) {
      errors.repPassword = 'Passwords do not match'
    }
    return errors
  }
}

module.exports = {
  validate
}
