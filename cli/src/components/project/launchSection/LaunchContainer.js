import React, {Component} from 'react';
import {connect} from 'react-redux';
import LaunchControl from './LaunchControl'
import {
  getProjectResultAction,
  startProjectAction,
  approveNewSnapshotAction,
  failProjectAction,
} from '../../actions/project';

class LaunchContainer extends Component {

  onClickApproveNewSnapshot(projectId, caseId, clientId){
    this.props.approveNewSnapshotAction(projectId, caseId, clientId, (path) => {
      this.props.history.push(path)
    })
  }

  onClickFailProject(projectId){
    this.props.failProjectAction(projectId, (path) => {
      this.props.history.push(path)
    })
  }

  onClickStartProject(id) {
    this.props.startProjectAction(id, (path) => {
      this.props.history.push(path)
    })
  }

  componentDidMount() {
    this.props.getProjectResultAction()
  }

  render() {
    const {initialValues, projectResult} = this.props.project;
    return (
      <LaunchControl
        initialValues={initialValues}
        projectResult={projectResult}
        onClickStartProject={this.onClickStartProject.bind(this)}
        onClickApproveNewSnapshot={this.onClickApproveNewSnapshot.bind(this)}
        onClickFailProject={this.onClickFailProject.bind(this)}
      />
    )
  }
}

function mapStateToProps({project}) {
  return {project};
}

export default connect(
  mapStateToProps,
  {
    getProjectResultAction,
    startProjectAction,
    approveNewSnapshotAction,
    failProjectAction,
  }
)(LaunchContainer);
