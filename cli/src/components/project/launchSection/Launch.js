import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Fab from "@material-ui/core/Fab/Fab";
import Launch from '@material-ui/icons/Launch';
import PlayIcon from '@material-ui/icons/PlayCircleFilled';
import AttentionIcon from '@material-ui/icons/PriorityHigh';
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Approve from './Approve'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  fab: {
    margin: theme.spacing.unit,
    left: theme.spacing.unit * 80,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  column: {
    margin: 5,
    padding: 5,
    width: 110,
  },
  button: {
    margin: theme.spacing.unit,
  },
  greenButton: {
    margin: theme.spacing.unit,
    color: green[500],
  },
  redIcon: {
    color: red[500],
  },
});

let id = 0;
function createData(projectCase, client, mismatch, error, errorMessage) {
  id += 1;
  return { projectCase, client, mismatch, error, errorMessage };
}

const rows = [
  createData('Cs 1', 'Cl 1', 12000, 'ok', 'bad errors'),
  createData('Cs 1', 'Cl 2', 32000, 'ok', 'bad errors'),
  createData('Cs 2', 'Cl 1', 42000, 'ok', 'bad errors'),
  createData('Cs 2', 'Cl 2', 52000, 'ok', 'bad errors'),
];

class SimpleTable extends React.Component {
  state = {
    item: {},
    open: false,
    projectId: '',
    caseId: '',
    clientId: ''
  };

  approveHandle = (projectId, caseId, clientId) => {
    this.setState({
      open: true,
      projectId: projectId,
      caseId: caseId,
      clientId: clientId
    });
  }

  handleClose = value => {
    this.setState({open: false});
  };

  render() {
    const {
      onClickStartProject,
      classes,
      initialValues,
      projectResult,
      onClickApproveNewSnapshot,
      onClickFailProject,
    } = this.props;
    return (
      <div>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell align="center" className={classes.column}>Case</TableCell>
              <TableCell align="center" className={classes.column}>Client</TableCell>
              <TableCell align="center" className={classes.column}>Mismatch</TableCell>
              <TableCell align="center" className={classes.column}>Last error message</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {projectResult.map(row => (
              <TableRow key={row.caseId + row.clientName}>
                <TableCell component="th" scope="row" align="center" className={classes.column}>
                  <Button color="primary" className={classes.button}>
                    {row.caseName}
                  </Button>
                </TableCell>
                <TableCell align="center" className={classes.column}>
                  <Button color="primary" className={classes.button}>
                    {row.clientName}
                  </Button>
                </TableCell>
                <TableCell align="center" className={classes.column}>
                  {row.mismatch === 0 && (
                    <Button
                      className={classes.greenButton}>
                      {row.mismatch}
                    </Button>
                  )}
                  {row.mismatch > 0 && (
                    <Button
                      color="secondary"
                      className={classes.button}
                      onClick={() => {
                        this.approveHandle(initialValues.id, row.caseId, row.clientId)}}
                    >
                      {row.mismatch}
                    </Button>
                  )}
                </TableCell>
                <TableCell align="center" className={classes.column}>{row.lastErrorMessage}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Fab color="primary" aria-label="Add" className={classes.fab} onClick={() => {
          onClickStartProject(initialValues.id)
        }}>
          <PlayIcon/>
        </Fab>
        <Approve
          onClickApproveNewSnapshot={onClickApproveNewSnapshot}
          projects={this.props.projects}
          selectedValue={this.state.selectedValue}
          open={this.state.open}
          projectId={this.state.projectId}
          caseId={this.state.caseId}
          clientId={this.state.clientId}
          onClose={this.handleClose}
          fullScreen={true}
          fullWidth={true}
          maxWidth={true}
          onClickFailProject={onClickFailProject}
      />
      </div>
    );
  }
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);
