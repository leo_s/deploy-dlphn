import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import blue from "@material-ui/core/colors/blue";
import {withStyles} from "@material-ui/core";
import PropTypes from "prop-types";
import ApproveControl from './ApproveControl';

const styles = {
  button: {
    margin: 5,
  },
  dialog:{
    fullWidth: true,
    maxWidth: 'xl',
    fullScreen: true
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
};

class AlertDialog extends React.Component {
  state = {
    open: false,
    images: ''
  };

  render() {
    const {
      selected,
      classes,
      open,
      onClose,
      onClickApproveNewSnapshot,
      projectId,
      caseId,
      clientId,
      onClickFailProject
    } = this.props;
    return (
      <div>

        <Dialog
          fullWidth={true}
          maxWidth={'sm'}
          className={classes.dialog}
          open={open}
          onClose={onClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Approve the item"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <ApproveControl
                projectId={projectId}
                caseId={caseId}
                clientId={clientId}
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={() => {onClickFailProject(projectId)}}
            >
              Fail
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => {onClickApproveNewSnapshot(projectId, caseId, clientId)}}
            >
              Approve
            </Button>
            <Button onClick={onClose} color="primary" autoFocus>
              Exit
            </Button>
          </DialogActions>
        </Dialog>

      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};

const AlertDialogWrapped = withStyles(styles)(AlertDialog);

export default AlertDialogWrapped;
