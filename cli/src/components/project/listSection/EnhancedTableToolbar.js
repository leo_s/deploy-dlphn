import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddCircle from '@material-ui/icons/AddCircle';
import {lighten} from "@material-ui/core/styles/colorManipulator";
import PowerIcon from '@material-ui/icons/Power';
import PlayIcon from '@material-ui/icons/PlayCircleFilled';
import green from "@material-ui/core/colors/green";

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight: {
    color: theme.palette.primary.main,
    backgroundColor: lighten(theme.palette.primary.light, 0.90),
  },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.primary,
    width: 500,
  },
  title: {
    flex: '0 0 auto',
  },
  greenIcon: {
    color: green[500],
  }
});

let EnhancedTableToolbar = props => {
  const {
    onClickAddProject,
    numSelected,
    classes,
    selected,
    delButtonHandle,
    onClickEditProject,
    onClickLaunchProject
  } = props;
  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 100 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            Projects
          </Typography>
        )}
      </div>
      <div className={classes.spacer}/>
      <div className={classes.actions}>
        {numSelected < 1 ? (
          <div>
            <Tooltip title="">
              <IconButton onClick={() => {onClickAddProject()}} >
                <AddCircle/>
              </IconButton>
            </Tooltip>
            <Tooltip title="">
              <IconButton>
                <DeleteIcon color="disabled"/>
              </IconButton>
            </Tooltip>
            <Tooltip title="">
              <IconButton aria-label="">
                <EditIcon color="disabled"/>
              </IconButton>
            </Tooltip>
            <Tooltip title="">
              <IconButton aria-label="">
                <PlayIcon color="disabled"/>
              </IconButton>
            </Tooltip>
          </div>
        ) : (numSelected === 1 ? (
            <div>
              <Tooltip title="">
                <IconButton onClick={() => {
                  onClickAddProject()}} >
                  <AddCircle/>
                </IconButton>
              </Tooltip>
              <Tooltip title="">
                <IconButton onClick={() => {
                  delButtonHandle()}} >
                  <DeleteIcon/>
                </IconButton>
              </Tooltip>
              <Tooltip title="">
                <IconButton onClick={() => {
                  onClickEditProject(selected)}} >
                  <EditIcon/>
                </IconButton>
              </Tooltip>
              <Tooltip title="">
                <IconButton onClick={() => {
                  onClickLaunchProject(selected)}} >
                  <PlayIcon color="primary"/>
                </IconButton>
              </Tooltip>
            </div>
          ) : (
            <div>
              <Tooltip title="">
                <IconButton onClick={() => {
                  onClickAddProject()}} >
                  <AddCircle/>
                </IconButton>
              </Tooltip>
              <Tooltip title="">
                <IconButton>
                  <DeleteIcon color="disabled"/>
                </IconButton>
              </Tooltip>
              <Tooltip title="">
                <IconButton aria-label="">
                  <EditIcon color="disabled"/>
                </IconButton>
              </Tooltip>
              <Tooltip title="">
                <IconButton aria-label="">
                  <PowerIcon color="disabled"/>
                </IconButton>
              </Tooltip>
            </div>
          )
        )}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
};

export default EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);
