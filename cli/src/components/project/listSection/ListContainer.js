import React, {Component} from 'react';
import {connect} from 'react-redux';
import ListControl from './ListControl'
import {
  showProjectNameForm,
  postProjectAction,
  getProjectsAction,
  editSectionRedirectionAction,
  launchSectionRedirectionAction,
  deleteProjectAction,
} from '../../actions/project';


class ProjectContainer extends Component {

  onClickPostProject(values) {
    this.props.postProjectAction(values, (path) => {
      this.props.history.push(path)
    })
  }

  onClickAddProject(){
    this.props.showProjectNameForm()
  }
  onClickDeleteProject(selected){
    this.props.deleteProjectAction(selected, (path) => {
      this.props.history.push(path)
    })
  }
  onClickEditProject(selected){
    this.props.editSectionRedirectionAction(selected, (path) => {
      this.props.history.push(path)
    })
  }

  onClickLaunchProject(selected){
    this.props.launchSectionRedirectionAction(selected, (path) => {
      this.props.history.push(path)
    })
  }
  componentDidMount() {
    this.props.getProjectsAction();
  }

  render() {
    const {renderPage, projects} = this.props.project;

    return (
      <ListControl
            projects={projects}
            onClickAddProject={this.onClickAddProject.bind(this)}
            onClickEditProject={this.onClickEditProject.bind(this)}
            onClickLaunchProject={this.onClickLaunchProject.bind(this)}
            renderPage={renderPage}
            onClickPostProject={this.onClickPostProject.bind(this)}
            onClickDeleteProject={this.onClickDeleteProject.bind(this)}
            getProjects={this.props.getProjectsAction.bind(this)}
      />
    )
  }
}

function mapStateToProps({project}) {
  return {project};
}

export default connect(
  mapStateToProps,
  {
    showProjectNameForm,
    postProjectAction,
    getProjectsAction,
    editSectionRedirectionAction,
    launchSectionRedirectionAction,
    deleteProjectAction,
  }
)(ProjectContainer);
