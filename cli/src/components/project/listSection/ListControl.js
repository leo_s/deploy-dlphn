import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import green from '@material-ui/core/colors/green';
import Grid from "@material-ui/core/Grid/Grid";
import ProjectList from "./ProjectList";
import ProjectForm from './ListForm'

function TabContainer(props) {
  const { children, dir } = props;

  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    margin: 50,
    backgroundColor: theme.palette.background.paper,
    width: 800,
    position: 'relative',
    minHeight: 200,
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
});

class FloatingActionButtonZoom extends React.Component {
  state = {
    value: 0,
    projectListClick: 'list'
  };

  handleListClick = (selected) => {
    if (selected === 1) {
      this.setState({ projectListClick: 'item' });
    }
    else {
      this.setState({ projectListClick: 'list' });
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const {
      onClickAddProject,
      onClickEditProject,
      onClickLaunchProject,
      onClickPostProject,
      renderPage,
      projects,
      classes,
      theme,
      onClickDeleteProject,
      getProjects
    } = this.props;

    let expose;
    if (renderPage.localeCompare('showList') === 0) {
      expose = <ProjectList
        onClickDeleteProject={onClickDeleteProject}
        onClickAddProject={onClickAddProject}
        onClickEditProject={onClickEditProject}
        onClickLaunchProject={onClickLaunchProject}
        projects={projects}
        handleListClick={this.handleListClick}
        getProjects={getProjects}
      />
    } else if (renderPage.localeCompare('showForm' === 0)) {
      expose =
        <ProjectForm
        onClick={onClickPostProject}
        />
    }

    return (
      <Grid container spacing={24}>
        <Grid item xs={3}>
        </Grid>
        <Grid item xs={8}>
          <div className={classes.root}>
            <AppBar position="static" color="default">
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
              >
                <Tab label="Project list" />
              </Tabs>
            </AppBar>
            <SwipeableViews
              axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
              index={this.state.value}
              onChangeIndex={this.handleChangeIndex}
            >
              <TabContainer dir={theme.direction}>
                {expose}
              </TabContainer>
            </SwipeableViews>
          </div>
        </Grid>
        <Grid item xs={1}>
        </Grid>
      </Grid>
    );
  }
}

FloatingActionButtonZoom.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(FloatingActionButtonZoom);
