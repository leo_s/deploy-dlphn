import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import blue from "@material-ui/core/colors/blue";
import {withStyles} from "@material-ui/core";
import PropTypes from "prop-types";

const styles = {
  button: {
    margin: 5,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
};

class AlertDialog extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { projects, selected, classes, open, onClose, onClickDeleteProject } = this.props;
    return (
      <div>

        <Dialog
          open={open}
          onClose={onClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Project disposal"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure you want to delete
              <Typography variant="h6" gutterBottom>
                {selected.map((index, key) => {
                    return projects.map((projectIndex, projectKey) => {
                      if (projectIndex.id === index){
                        return(
                          projectIndex.name
                        )
                      } return null
                    })
                  }
                )}
              </Typography>
               project.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              className={classes.button}
                    variant="contained"
                    color="secondary"
                    onClick={() => {onClickDeleteProject(selected)}}
            >
              Delete
            </Button>
            <Button onClick={onClose} color="primary" autoFocus>
              Exit
            </Button>
          </DialogActions>
        </Dialog>

      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};

const AlertDialogWrapped = withStyles(styles)(AlertDialog);

export default AlertDialogWrapped;
