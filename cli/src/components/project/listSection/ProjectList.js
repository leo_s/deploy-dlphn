import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Confirm from './Confirm';
import EnhancedTableToolbar from './EnhancedTableToolbar';
import Divider from "@material-ui/core/Divider/Divider";
import EnhancedTableHead from './EnhancedTableHead';
import DoneIcon from '@material-ui/icons/Done';
import ContactSupport from '@material-ui/icons/ContactSupport';
import AttentionIcon from '@material-ui/icons/PriorityHigh';
import UpdateIcon from '@material-ui/icons/Update';
import IconButton from "@material-ui/core/IconButton/IconButton";
import green from "@material-ui/core/colors/green";
import orange from "@material-ui/core/colors/orange";
import red from "@material-ui/core/colors/red";

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const styles = theme => ({
  root: {
    width: '100%',
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  greenIcon: {
    color: green[500],
  },
  orangeIcon: {
    color: orange[500],
  },
  redIcon: {
    color: red[700],
  },
});

class EnhancedTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'name',
    selected: [],
    data: [],
    page: 0,
    rowsPerPage: 5,
    open: false,
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({order, orderBy});
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({selected: this.props.projects.map(n => n.id)}));
      return;
    }
    this.setState({selected: []});
  };

  handleClick = (event, id) => {
    const {selected} = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({selected: newSelected});
    this.props.handleListClick(newSelected.length);
  };

  handleChangePage = (event, page) => {
    this.setState({page});
  };

  handleChangeRowsPerPage = event => {
    this.setState({rowsPerPage: event.target.value});
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  deleteHandle = () => {
    this.setState({open: true});
  }

  handleClose = value => {
    this.setState({open: false});
  };

  componentDidMount() {
      this.timerID = setInterval(
        () => this.props.getProjects(),
        1000
      );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  render() {
    const {
      classes,
      projects=[],
      onClickEditProject,
      onClickDeleteProject,
      onClickLaunchProject
    } = this.props;
    const {order, orderBy, selected=[], rowsPerPage, page} = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, projects.length - page * rowsPerPage);

    return (
      <div className={classes.root}>
            <Paper className={classes.root}>
              <EnhancedTableToolbar
                onClickAddProject={this.props.onClickAddProject}
                numSelected={selected.length}
                selected={selected}
                delButtonHandle={this.deleteHandle}
                onClickEditProject={onClickEditProject}
                onClickLaunchProject={onClickLaunchProject}
              />
              <Divider />
              <div className={classes.tableWrapper}>
                <Table className={classes.table} aria-labelledby="tableTitle">
                  <EnhancedTableHead
                    numSelected={selected.length}
                    order={order}
                    orderBy={orderBy}
                    onSelectAllClick={this.handleSelectAllClick}
                    onRequestSort={this.handleRequestSort}
                    rowCount={projects.length}
                  />
                  <TableBody>
                    {stableSort(projects, getSorting(order, orderBy))
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map(n => {
                        const isSelected = this.isSelected(n.id);
                        return (
                          <TableRow
                            hover
                            onClick={event => this.handleClick(event, n.id)}
                            role="checkbox"
                            aria-checked={isSelected}
                            tabIndex={-1}
                            key={n.id}
                            selected={isSelected}
                          >
                            <TableCell padding="checkbox">
                              <Checkbox checked={isSelected}/>
                            </TableCell>
                            <TableCell component="th" scope="row" padding="none">
                              {n.name}
                            </TableCell>
                            <TableCell align="left">{n.description}</TableCell>
                            <TableCell align="left">
                              {n.status && n.status.localeCompare('Pass') === 0 && (
                                <IconButton><DoneIcon className={classes.greenIcon}/></IconButton>
                              )}
                              {n.status && n.status.localeCompare('In Process') === 0 && (
                                <IconButton><UpdateIcon color="primary"/></IconButton>
                              )}
                              {n.status && n.status.localeCompare('Need Validation') === 0 && (
                                <IconButton><ContactSupport className={classes.orangeIcon}/></IconButton>
                              )}
                              {n.status && n.status.localeCompare('Fail') === 0 && (
                                <IconButton><AttentionIcon className={classes.redIcon}/></IconButton>
                              )}
                              </TableCell>
                            <TableCell align="left">{n.duration}</TableCell>
                          </TableRow>
                        );
                      })}
                    {emptyRows > 0 && (
                      <TableRow style={{height: 49 * emptyRows}}>
                        <TableCell colSpan={6}/>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </div>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={projects.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </Paper>
            <Confirm
              onClickDeleteProject={onClickDeleteProject}
              projects={this.props.projects}
              selected={selected}
              selectedValue={this.state.selectedValue}
              open={this.state.open}
              onClose={this.handleClose}
            />
      </div>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EnhancedTable);
