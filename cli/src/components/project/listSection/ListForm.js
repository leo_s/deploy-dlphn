import React from 'react';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import indigo from "@material-ui/core/colors/indigo";
import red from "@material-ui/core/colors/red";
import grey from "@material-ui/core/colors/grey";
import FormControl from "@material-ui/core/FormControl/FormControl";
import connect from "react-redux/es/connect/connect";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  title: {
    marginBottom: -10,
    color: indigo[700],
  },
  error: {
    marginTop: 10,
    marginBottom: -10,
    color: red[500],
  },
  card: {
    margin: 40,
    background: grey[100],
  },
  form: {
    marginLeft: 70,
    marginRight: 70,
  },
  button: {
    marginTop: 15,
  },
});

const renderTextField = ({
                           label,
                           input,
                           meta: {touched, invalid, error},
                           ...custom
                         }) => {
  return(
  <FormControl fullWidth={true}>
  <TextField
    label={label}
    placeholder={label}
    error={touched && invalid}
    helperText={touched && error}
    margin="normal"
    {...input}
    {...custom}
  />
  </FormControl>
)}

function ListForm(props) {
  const {
    onClick,
    classes,
  } = props;
  return (
    <div className={classes.root}>
          <Card className={classes.card}>
            <CardContent className={classes.form}>
              <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                Project
              </Typography>
              {props.error && <Typography gutterBottom variant="subtitle2" component="h2" className={classes.error}>
                ERROR: {props.error}
                </Typography>}

              <form onSubmit={props.handleSubmit(onClick)} id="form_login">
                <div>
                  <Field
                    name="name"
                    component={renderTextField}
                    label="Provide the Project name"/>
                </div>
                <div>
                  <Field
                    name="description"
                    component={renderTextField}
                    label="Provide the description"/>
                </div>
                <div className={classes.button}>
                  <Button type="submit" variant="contained" color="primary" name="form__button_submit"
                          disabled={props.pristine || props.submitting}>
                    Submit
                  </Button>
                </div>
              </form>
            </CardContent>
          </Card>
    </div>
  )
}

ListForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

ListForm = (reduxForm({
  form: 'ListForm',
})(ListForm));

const selector = formValueSelector('ListForm');
ListForm = connect(state => {
  const listTypeValue = selector(state, 'listType');
  return {
    listTypeValue,
  }
})(ListForm);

export default withStyles(styles)(ListForm)
