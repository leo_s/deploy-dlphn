import React, {Component} from 'react';
import {connect} from 'react-redux';
import EditControl from './EditControl'
import {
  getProjectAction,
  putProjectAction
} from '../../actions/project';

class EditContainer extends Component {

  onClickPutProject(values) {
    this.props.putProjectAction(values, (path) => {
          this.props.history.push(path)
        })
  }

  componentDidMount() {
    this.props.getProjectAction()
  }

  render() {
    const {initialValues} = this.props.project;
    return (
      <EditControl
        initialValues={initialValues}
        onClickPutProject={this.onClickPutProject.bind(this)}

      />
    )
  }
}

function mapStateToProps({project}) {
  return {project};
}

export default connect(
  mapStateToProps,
  {
    getProjectAction,
    putProjectAction,
  }
)(EditContainer);
