import React from 'react';
import {Field} from 'redux-form';
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import Grid from "@material-ui/core/Grid/Grid";
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import indigo from "@material-ui/core/colors/indigo";
import grey from "@material-ui/core/colors/grey";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  title: {
    marginBottom: -10,
    color: indigo[700],
  },
  card: {
    margin: 40,
    background: grey[100],
  },
  form: {
    marginLeft: 70,
    marginRight: 70,
  },
  button: {
    marginTop: 15,
  },

});

const renderTextField = ({
                           label,
                           input,
                           meta: {touched, invalid, error},
                           ...custom
                         }) => (
  <TextField
    label={label}
    placeholder={label}
    error={touched && invalid}
    helperText={touched && error}
    margin="normal"
    {...input}
    {...custom}
  />
)

function LoginForm(props) {

  const {classes} = props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={5}>
          </Grid>
          <Grid item xs={4}>
            <Card className={classes.card}>
              <CardContent className={classes.form}>
                <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                  Login
                </Typography>
                  <form onSubmit={props.onSubmit} id="form_login">
                  <div>
                    <Field name="email" component={renderTextField} label="Provide an e-mail"/>
                  </div>
                  <div>
                    <Field name="password" type="password" component={renderTextField} label="Provide password"/>
                  </div>
                  <div className={classes.button}>
                    <Button type="submit" variant="contained" color="primary" name="form__button_submit"
                      disabled={props.pristine || props.submitting}>
                      Submit
                    </Button>
                  </div>
                </form>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={3}>
          </Grid>
        </Grid>
      </div>
    )
}

export default withStyles(styles)(LoginForm)
