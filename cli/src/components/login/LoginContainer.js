import React, {Component} from 'react';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {loginAction} from '../actions/';
import Login from './Login'

class LoginForm extends Component {

  onSubmit(values) {
    this.props.loginAction(values, (path) => {
      this.props.history.push(path)
    })
  }

  render() {
    return (
      <Login
        onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}
        pristine={this.props.pristine}
        submitting={this.props.submitting}
      />
    )
  }
}

export default connect(
  null,
  {loginAction}
)(reduxForm({
  form: 'LoginForm',
})(LoginForm));
