import React, {Component} from 'react';
import './App.scss';
import {BrowserRouter} from 'react-router-dom';
import Drawer from './components/drawer/Drawer'
import Main from './components/main/Main'
import {applyMiddleware, createStore} from "redux";
import reducers from "./components/reducers";
import {
  initialAuthState,
  initialProfileState,
  initialProjectState,
  initialClientState,
  initialCaseState,
  initialExclusionState,
  initialTokenState,
} from "./initialState";
import reduxThunk from "redux-thunk";
import {Provider} from 'react-redux';

const store = createStore(reducers, {
  auth: initialAuthState,
  profile: initialProfileState,
  project: initialProjectState,
  client: initialClientState,
  projectCase: initialCaseState,
  projectExclusion: initialExclusionState,
  tokenReducer: initialTokenState
}, applyMiddleware(reduxThunk));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Drawer/>
            <Main/>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
