
Feature('Signup page');

Before((I) => {
  I.amOnPage('/signup');
});

Scenario('Test title', (I) => {
  I.see('Signup');
  I.fillField('name', 'Gek');
  I.fillField('email', '3333@gmail.com');
  I.fillField('password', '1qaz@WSX');
  I.fillField('repPassword', '1qaz@WSX');
  I.click('form__button_submit', '#form_signup');
  I.waitForElement('#page_user');
  I.see('3333@gmail.com');
});
