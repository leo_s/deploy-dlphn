
Feature('Login page');

Before((I) => {
  I.amOnPage('http://nginx:80/login');
});

Scenario('Test login', (I) => {
  I.see('Login');
  I.fillField('email', '3333@gmail.com');
  I.fillField('password', '1qaz@WSX');
  I.click('form__button_submit', '#form_login');
  I.waitForElement('#page_user');
  I.see('3333@gmail.com');
});
