var request = require('sync-request');
const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 9222;
const url = `http://${host}:${port}/json/version`;
const response = request('GET', url);
const json = JSON.parse(response.body.toString('utf8'));
const browserWSEndpoint = json.webSocketDebuggerUrl;

exports.config = {
  tests: 'tests/*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://nginx',
      chrome: {
        browserWSEndpoint: browserWSEndpoint
      }
    }
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'tests'
}
