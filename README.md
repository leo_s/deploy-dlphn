# About Project

This is snap checker service. It provides UI and api for testing web app by snapshots.

#### Requirements
1) Unix (Linux / MacOS), Windows - need adaptive shell commands 
2) Docker - see https://docs.docker.com/install/
3) ahoy - see https://github.com/ahoy-cli/ahoy

#### Setup

1) add 'snapchecker.local  127.0.0.1' to hosts file
2) ahoy init

#### Run

ahoy up

#### Test
cd ./ops-tools/docker
docker-compose start db
docker-compose start api
docker-compose exec api sh -c "npm test"
docker-compose stop api
docker-compose stop db

#### Usage

Open in browser "http://snapchecker.local:8080"

For more commands see .ahoy

#### Conventions

##### git
We are using standard git workflow. Master branch for release. Develop branch for main work.
When you need to implement a new feature developer create a new branch from develop branch.
He named it like "feature/{{ featureTitle }}".
When you need to fix bug developer create a new branch from develop branch.
He named it like "fix/{{ fixTitle }}".
When developer finished job on his issue, he creates Pull Request and assigns project maintainer.
